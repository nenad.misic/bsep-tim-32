import React, {Component} from 'react';
import siemService from '../../Services/siemService';
import { Redirect } from 'react-router-dom'

class AddSiem extends Component {
  constructor(props) {
    super(props);
    this.id = '';
    this.state = {
      organization: '',
      organizationalUnit: '',
      commonName: '',
      country: '',
      state: '',
      locality: '',
      passphrase: 0,
      val_organization: true,
      val_organizationalUnit: true,
      val_commonName: true,
      val_country: true,
      val_state: true,
      val_locality: true,
      val_passphrase: true,
      regex_organization: (real) => new RegExp('.+').test(real),
      regex_organizationalUnit: (real) => true,
      regex_commonName: (real) => true,
      regex_country: (real) => new RegExp('.+').test(real) && real.length == 2,
      regex_state: (real) => true,
      regex_locality: (real) => true,
      regex_passphrase: (real) => new RegExp('.+').test(real) && real.length >= 5,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.input = React.createRef();
  }
  
  async componentDidMount() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('siem_admin'))
    {
      return;
    }
    const { match: { params } } = this.props;
    this.id = params.id;
  }

  handleChange = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val, [`val_${nam}`]: this.state[`regex_${nam}`](val)});
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    if(!this.everythingOkay()) return;
    let response = await siemService.add({
        name: `O=${this.state.organization}, OU=${this.state.organizationalUnit}, CN=${this.state.commonName}, C=${this.state.country}, ST=${this.state.state}, L=${this.state.locality}`,
        password: this.state.passphrase,
        parent: +this.id,
    });

    alert('Successfully added new siem center.');

  }
  
  everythingOkay = () => {
    return this.state.regex_organization(this.state.organization) &&
      this.state.regex_organizationalUnit(this.state.organizationalUnit) &&
      this.state.regex_commonName(this.state.commonName) &&
      this.state.regex_country(this.state.country) &&
      this.state.regex_state(this.state.state) &&
      this.state.regex_locality(this.state.locality) &&
      this.state.regex_passphrase(this.state.passphrase)
  }
  
  render() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('siem_admin'))
    {
      return <Redirect to='/unauthorized' />
    }
      
    return (
      <div className="offset-lg-4 offset-md-2 col-lg-4 col-md-8 col-12">
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="organization">Organization</label>
            <input type="text" className="form-control" id="organization" aria-describedby="organizationHelp" placeholder="Enter organization" name="organization" onChange={this.handleChange}/> 
            <span class="valerror" style={{opacity: this.state.val_organization?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
            <label htmlFor="organizationalUnit">Organizational Unit</label>
            <input type="text" className="form-control" id="organizationalUnit" aria-describedby="organizationalUnitHelp" placeholder="Enter organizational unit" name="organizationalUnit" onChange={this.handleChange}/> 
            <span class="valerror" style={{opacity: this.state.val_organizationalUnit?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
            <label htmlFor="commonName">Common name</label>
            <input type="text" className="form-control" id="commonName" aria-describedby="commonNameHelp" placeholder="Enter common name" name="commonName" onChange={this.handleChange}/> 
            <span class="valerror" style={{opacity: this.state.val_commonName?0:1}}>Validation fail</span>
           </div>
          <div className="form-group">
            <label htmlFor="country">Country</label>
            <input type="text" className="form-control" id="country" aria-describedby="countryHelp" placeholder="Enter country" name="country" onChange={this.handleChange}/> 
            <span class="valerror" style={{opacity: this.state.val_country?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
            <label htmlFor="state">State</label>
            <input type="text" className="form-control" id="state" aria-describedby="stateHelp" placeholder="Enter state" name="state" onChange={this.handleChange}/>
            <span class="valerror" style={{opacity: this.state.val_state?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
            <label htmlFor="locality">Locality</label>
            <input type="text" className="form-control" id="locality" aria-describedby="localityHelp" placeholder="Enter locality" name="locality" onChange={this.handleChange}/> 
            <span class="valerror" style={{opacity: this.state.val_locality?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
            <label htmlFor="passphrase">Passphrase</label>
            <input type="password" className="form-control" id="passphrase" placeholder="Passphrase" name="passphrase" onChange={this.handleChange}/>
            <span class="valerror" style={{opacity: this.state.val_passphrase?0:1}}>Validation fail</span>
          </div>
          <button type="submit" disabled={!this.everythingOkay()} className="btn btn-primary col-8 offset-2 my-5">Submit</button>
        </form>
      </div>
    );
  }
}
export default AddSiem;