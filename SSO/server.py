import bcrypt
from flask import Flask, jsonify, request
from flask_jwt_extended import (
    JWTManager, create_access_token, decode_token
)

from flask_cors import CORS
from User import User
import db_service
import datetime
app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'super-secret'
jwt = JWTManager(app)

# cors = CORS(app, resources={r"/*": {"origins": "http://localhost:3000"}})
cors = CORS(app, resources={r"/*": {"origins": "*"}})

def get_hashed_password(plain_text_password):
    # Hash a password for the first time
    #   (Using bcrypt, the salt is saved into the hash itself)
    return bcrypt.hashpw(plain_text_password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')

def check_password(plain_text_password, hashed_password):
    # Check hashed password. Using bcrypt, the salt is saved into the hash itself
    return bcrypt.checkpw(plain_text_password.encode('utf-8'), hashed_password.encode('utf-8'))

@app.route('/auth', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)
    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    user = db_service.get_user_by_username(username)

    if user is None or (not check_password(password, user.password)):
        return jsonify({"msg": "Wrong username or password"}), 400

    expires = datetime.timedelta(days=2)
    access_token = create_access_token(identity=user.to_dict(), expires_delta=expires)
    return jsonify(access_token=access_token), 200



@app.route('/decode', methods=['POST'])
def decode():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    token = request.json.get('token', None)
    if not token:
        return jsonify({"msg": "Missing token parameter"}), 400

    # Identity can be any data that is json serializable
    json = decode_token(token)
    return jsonify(json), 200


@app.route('/register', methods=['POST'])
def register():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400

    password = request.json.get('password', None)
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    roles = request.json.get('roles', None)
    if not roles:
        return jsonify({"msg": "Missing roles parameter"}), 400

    siem_center_id = request.json.get('siem_center_id', None)

    ca_skid = request.json.get('ca_skid', None)



    db_service.save_user(User(username, get_hashed_password(password), roles, siem_center_id, ca_skid))
    return jsonify({"msg": "success"}), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)