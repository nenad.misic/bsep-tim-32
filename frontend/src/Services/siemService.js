
import axios from 'axios';
import {api_siem, bearer} from '../Environment/environment';
import { resolve } from 'dns';

let siemService = {
    getAll: async () => {
        // let url = `${api_pki}ca`;
        // return axios.get(url);

        let res = await axios.get(`${api_siem}siem`, bearer())
        return res
    },
    getById: async (id) => {
        // let url = `${api_pki}certificate/${name}`;
        // return axios.get(url);
        let res = await axios.get(`${api_siem}siem/${id}`, bearer())
        return res
    },
    add: (obj) => {
        let url = `${api_siem}siem`;
        return axios.post(url, obj, bearer());
    },
    getLogs: (id) => {
        let res = axios.get(`${api_siem}logs/${id}`, bearer())
        return res
    },
    getAlarms: (id) => {
        let res = axios.get(`${api_siem}alarms/${id}`, bearer())
        return res
    },
    getLogsDate: (id, gt, lt) => {
        let res = axios.post(`${api_siem}logsDate/${id}`, {lt: lt.getTime(), gt: gt.getTime()}, bearer())
        return res
    },
    getAlarmsDate: (id, gt, lt) => {
        let res = axios.post(`${api_siem}alarmsDate/${id}`, {lt: lt.getTime(), gt: gt.getTime()}, bearer())
        return res
    },

    getUnapprovedAgents: (siemId) => {
        let res = axios.get(`${api_siem}unapproved/${siemId}`, bearer())
        return res
    },

    buildTree: (data) => {
        data.map(e => {
            e.children = data.filter(x => x.parent_siem === e.id && x.parent_siem !== x.id);
        });
        let root = data.filter(e => e.parent_siem === null)[0];
        return root;
    }
};

export default siemService
        
        
