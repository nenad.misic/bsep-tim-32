import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import SortableTree from 'react-sortable-tree';
import ReactSortableTreeThemeFileExplorer from 'react-sortable-tree-theme-full-node-drag';
import caService from '../Services/caService';
import certificateService from '../Services/certificateService';
import { Redirect } from 'react-router-dom'

class Certificates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detail: {},
      certificates: [],
    };
  }

  async componentDidMount() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('pki_admin'))
    {
      return;
    }
    var raw_data = (await caService.getAll()).data.data;
    raw_data = raw_data.map(e => {e.title = e.subject.split(',')[0].split('=')[1]; e.subtitle=`${new Date(e.not_before).toLocaleDateString()} - ${new Date(e.not_after).toLocaleDateString()}`; return e;});
    var data_tree = [caService.buildTree(raw_data)];
    // Kao ngOnInit
    // Na kraju setState da se okine render
    this.setState({
      detail: undefined,
      certificates: data_tree
    })  
  }

  revokeCa = async (e) => {
    await caService.revoke(this.state.detail.subjectKeyIdentifier);
    this.setState({
      detail: undefined
    })
  }

  revokeCert = async (subjectKeyIdentifier) => {
    await certificateService.revoke(subjectKeyIdentifier);
    let certificates = await certificateService.getAll(this.state.detail.subjectKeyIdentifier);
    let detailedNode = this.state.detail;
    detailedNode.certificates = certificates.data.data;
    this.setState({
      detail: detailedNode,
    })
  }

  render() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('pki_admin'))
    {
      return <Redirect to='/unauthorized' />
    }
    return (
      
      <div className="container-fluid">
        {/* {!this.state.certificates && <p>Loading certificates...</p>} */}
        {/* <div className="row">
          {
            this.state.certificates  && this.state.certificates.length > 0 && this.state.certificates.map(certificate => (
                  <div key={certificate.id} className="col-xs-12 col-sm-6 col-md-4 col-lg-2 d-flex align-items-stretch">
                  </div>
            )
          )
          }
        </div> */}

        <div className="row">
          <div className= "col-md-4 col-12 card" style={{ height: '84vh' }}>
            <SortableTree
              canDrag={false}
              treeData={this.state.certificates}
              onChange={certificates => this.setState({ certificates })}
              theme={ReactSortableTreeThemeFileExplorer}
              generateNodeProps={({ node, path }) => ({
                buttons: [
                  <button className="btn btn-primary"
                    onClick={async () => {
                      let certificates = await certificateService.getAll(node.subjectKeyIdentifier);
                      let detailedNodeData = await caService.getById(node.subjectKeyIdentifier);
                      let detailedNode = detailedNodeData.data;
                      detailedNode.certificates = certificates.data.data;
                      this.setState({
                        detail: detailedNode,
                      })
                    }
                  }
                  >
                    Details
                  </button>,
                ],
              })}
            />
          </div>

          {this.state.detail && this.state.detail.subject &&  
          <div className= "col-md-4 pr-0 col-12">
              <div className="pb-5 card col-12" style={{ height: 'fit-content' }}>
                <div className="row">
                  <div className="col-6">
                  {this.state.detail.subject.split('emailAddress=')[0].split`,`.map(e => (
                <h2 className="my-2">{e}</h2>
                ))}
                
                <p>{new Date(this.state.detail.not_before).toLocaleDateString()} - {new Date(this.state.detail.not_after).toLocaleDateString()}</p>
                <Link to={`/ca/${this.state.detail.subjectKeyIdentifier}`} className="card-link text-info">Details</Link>
                  </div>
                  <div  className="col-5 offset-1 my-auto " >
                    {this.state.detail.revoked && 
                      <img src="/revoked_stamp.svg" style={{width: '100%', height:'auto', 'transform': `rotate(13deg)`}}></img>
                    }
                  </div>

                </div>
                
                <hr className="my-4"></hr>
                <div className="my-4"><h3 className="d-inline">Issued to: </h3><p className="d-inline">{this.state.detail.subject}</p></div>
                <div className="my-4"><h3 className="d-inline">Issued by: </h3><p className="d-inline">{this.state.detail.issuer}</p></div>
                <div className="my-4"><h3 className="d-inline">Valid from: </h3><p className="d-inline">{new Date(this.state.detail.not_before).toLocaleDateString()}</p></div>
                <div className="my-4"><h3 className="d-inline">Valid to: </h3><p className="d-inline">{new Date(this.state.detail.not_after).toLocaleDateString()}</p></div>
                <hr className="my-4"></hr>
                <h3 className="my-4">Subject key identifier: </h3><p>{this.state.detail.subjectKeyIdentifier}</p>
                <br className="mb-5"></br>
                <div className="container">
                  <div className="row">
                    <div className="btn-group blocks col-md-12" >
                      <Link className="col-4 btn btn-success" to={`/add-ca/${this.state.detail.subjectKeyIdentifier}`}>
                        Add CA
                      </Link>
                      <Link className="col-4 btn btn-success" to={`/add-cert/${this.state.detail.subjectKeyIdentifier}`}>
                        Add Certificate
                      </Link>
                      <button className="col-4 btn btn-danger" onClick={this.revokeCa} disabled={this.state.detail.revoked}>Revoke</button>
                    </div>
                  </div>
                </div>
              </div>
              <div>
              </div>
          </div> 
          }
          {this.state.detail && this.state.detail.certificates && this.state.detail.certificates.length !== 0 &&
            <div className= "col-md-4 col-12">
            <div className="pb-5 col-12" style={{ height: 'fit-content' }}>
                    <h2>Certificates:</h2>
                  {this.state.detail.certificates.map(e => (<div className={`border radius-custom mb-3 row ${e.revoked ? 'border-danger' : 'card-border'}`}>
                    <div className="col-9">
                      <h5 className="card-title">{e.subject.split(',')[0].split('=')[1]}</h5>
                      <h6 className="card-subtitle mb-2 text-muted">{new Date(e.not_before).toLocaleDateString()} - {new Date(e.not_after).toLocaleDateString()}</h6>
                      <Link to={`/cert/${e.subjectKeyIdentifier}`} className="card-link text-info">Details</Link>
                      {!e.revoked && <a href="#" className="card-link text-danger" onClick={ () => this.revokeCert(e.subjectKeyIdentifier)}>Revoke</a>}
                      </div>
                      <div  className="col-3 my-auto " >
                    {e.revoked && 
                      <img src="/revoked_stamp.svg" style={{width: '100%', height:'auto'}}></img>
                    }
                  </div>
                  </div>
                    ))
                    }
                    </div>
                    
            </div>
            }
        </div>
        </div>
        
    );
  }
}

export default Certificates;