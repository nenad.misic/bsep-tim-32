// {
//     "agentId": false,
//     "level": "",
//     "source": false,
//     "eventId": -1,
//     "taskCategory": "",
//     "message": false,
//     "time": 0,
//     "count": 1,
//     "info_message": "info"
// }

import React, {Component} from 'react';
import ruleService from '../Services/ruleService';
import { Redirect } from 'react-router-dom'

class AddRule extends Component {
  constructor(props) {
    super(props);
    this.id = '';
    this.state = {
        "agentId": false,
        "level": '',
        "source": false,
        "eventId": -1,
        "taskCategory": '',
        "message": false,
        "time": 0,
        "count": 1,
        "info_message": '',
        "val_agentId": true,
        "val_level": true,
        "val_source": true,
        "val_eventId": true,
        "val_taskCategory": true,
        "val_message": true,
        "val_time": true,
        "val_count": true,
        "val_info_message": true,
        "regex_agentId": (real) => typeof(real) == typeof(true),
        "regex_level": (real) => real=='INFO'||real=='WARN'||real=='ERROR'||real=='',
        "regex_source": (real) => typeof(real) == typeof(true),
        "regex_eventId": (real) => true,
        "regex_taskCategory": (real) => true,
        "regex_message": (real) => typeof(real) == typeof(true),
        "regex_time": (real) => real >= 0,
        "regex_count": (real) => real >= 1,
        "regex_info_message": (real) => new RegExp('.+').test(real),
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.input = React.createRef();
  }
  
  async componentDidMount() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('siem_admin'))
    {
      return;
    }
    const { match: { params } } = this.props;
    this.id = params.id;
  }

  handleChange = (event) => {
    let nam = event.target.name;
    let val = event.target.type=='checkbox'?event.target.checked:event.target.value;
    this.setState({[nam]: val, [`val_${nam}`]: this.state[`regex_${nam}`](val)});
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    if(!this.everythingOkay()) return;
    let response = await ruleService.addRule(
        {
            agentId: this.state.agentId,
            level: this.state.level,
            source: this.state.source,
            eventId: +this.state.eventId,
            taskCategory: this.state.taskCategory,
            message: this.state.message,
            time: +this.state.time,
            count: +this.state.count,
            info_message: this.state.info_message
        }
    );

    alert('Successfully added new rule to siem center.');

  }
  
  everythingOkay = () => {
    return this.state.regex_agentId(this.state.agentId) &&
      this.state.regex_level(this.state.level) &&
      this.state.regex_source(this.state.source) &&
      this.state.regex_eventId(this.state.eventId) &&
      this.state.regex_taskCategory(this.state.taskCategory) &&
      this.state.regex_message(this.state.message) &&
      this.state.regex_time(this.state.time) &&
      this.state.regex_count(this.state.count) &&
      this.state.regex_info_message(this.state.info_message);
  }
  
  render() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('siem_admin'))
    {
      return <Redirect to='/unauthorized' />
    }
    return (
      <div className="offset-lg-4 offset-md-2 col-lg-4 col-md-8 col-12">
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
              <label htmlFor="agentId">AgentId</label>
              <input type="checkbox" data-toggle="toggle" id="agentId" placeholder="Agent id" name="agentId" onChange={this.handleChange}></input>
              <span class="valerror" style={{opacity: this.state.val_agentId?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
              <label htmlFor="level">Level</label>
              <input type="text" className="form-control" id="level" placeholder="Level" name="level" onChange={this.handleChange}></input>
              <span class="valerror" style={{opacity: this.state.val_level?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
              <label htmlFor="source">Source</label>
              <input type="checkbox" data-toggle="toggle" id="source" placeholder="Source" name="source" onChange={this.handleChange}></input>
              <span class="valerror" style={{opacity: this.state.val_source?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
              <label htmlFor="eventId">EventId</label>
              <input type="number" className="form-control" id="eventId" placeholder="Event id" name="eventId" onChange={this.handleChange}></input>
              <span class="valerror" style={{opacity: this.state.val_eventId?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
              <label htmlFor="taskCategory">TaskCategory</label>
              <input type="text" className="form-control" id="taskCategory" placeholder="Task Category" name="taskCategory" onChange={this.handleChange}></input>
              <span class="valerror" style={{opacity: this.state.val_taskCategory?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
              <label htmlFor="message">Message</label>
              <input type="checkbox" data-toggle="toggle" id="message" placeholder="Message" name="message" onChange={this.handleChange}></input>
              <span class="valerror" style={{opacity: this.state.val_message?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
              <label htmlFor="time">Time</label>
              <input type="number" className="form-control" id="time" placeholder="Time in milliseconds" name="time" onChange={this.handleChange}></input>
              <span class="valerror" style={{opacity: this.state.val_time?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
              <label htmlFor="count">Count</label>
              <input type="number" className="form-control" id="count" placeholder="Count" name="count" onChange={this.handleChange}></input>
              <span class="valerror" style={{opacity: this.state.val_count?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
              <label htmlFor="info_message">Info_message</label>
              <input type="text" className="form-control" id="info_message" placeholder="Info message" name="info_message" onChange={this.handleChange}></input>
              <span class="valerror" style={{opacity: this.state.val_info_message?0:1}}>Validation fail</span>
          </div>
          <button type="submit" disabled={!this.everythingOkay()} className="btn btn-primary col-8 offset-2 my-5">Submit</button>
        </form>
      </div>
    );
  }
}
export default AddRule;