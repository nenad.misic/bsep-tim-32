import psycopg2
from pymongo import MongoClient

SYS_ADMIN_CONNECT_OPTIONS = "dbname='bsep' user='postgres' host='localhost' password='postgres'"
PKI_ADMIN_CONNECT_OPTIONS = "dbname='bsep' user='pki_admin' host='localhost' password='pki_admin'"


def get_siem_centers():
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.siem_center')
    result = cur.fetchall()
    conn.commit()
    cur.close()
    conn.close()
    return result

def get_siem_center(identifier):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.siem_center WHERE id=%s', (identifier,))
    result = cur.fetchone()
    conn.commit()
    cur.close()
    conn.close()
    return result

def add_siem_center(name, password, parent_siem):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()


    cur.execute('INSERT INTO bsep.pki.siem_center (name, \
        password, parent_siem) \
        VALUES (%s, %s, %s)',
        (name, password, parent_siem))


    conn.commit()
    cur.close()
    conn.close()

def get_agent_by_id(id):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.siem_agent WHERE id=%s', (id,))
    result = cur.fetchone()
    conn.commit()
    cur.close()
    conn.close()
    return result

def get_agents(siem_center_id):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.siem_agent WHERE siem_center=%s AND approved=true', (siem_center_id,))
    result = cur.fetchall()
    conn.commit()
    cur.close()
    conn.close()
    return result
    
def get_unapproved_agents(siem_center_id):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.siem_agent WHERE siem_center=%s AND approved=false', (siem_center_id,))
    result = cur.fetchall()
    conn.commit()
    cur.close()
    conn.close()
    return result

def approve_agent(identification, password):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('UPDATE bsep.pki.siem_agent SET approved=true, password=%s WHERE id=%s', (password, identification))
    conn.commit()
    cur.close()
    conn.close()

# logikos in case logs is reserved wordikos
def save_logs(logikos):
    result = db.logs.insert_many(logikos)

# logiko in case log is reserved wordiko
def save_log(logiko):
    result = db.logs.insert_one(logiko)

def get_agent_logs(agentId):
    result = db.logs.find({'agentId': agentId})
    return result

def get_logs_by_message(message):
    result = db.logs.find({'message': message})
    return result

def get_relevant_logs(agentId, message, timestamp):
    result = db.logs.find({'$or': [{'agentId': agentId}, {'message': message}]})
    return result

def save_alarms(alarms):
    result = db.alarms.insert_many(alarms)

def get_agent_logs_report(agentId):
    result = db.logs.find({'agentId': int(agentId)}).sort("_id", 1).limit(100)
    return result

def get_all_logs_report(siemId):
    
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT id FROM bsep.pki.siem_agent WHERE siem_center=%s', siemId)
    res = cur.fetchall()
    conn.commit()
    cur.close()
    conn.close()

    ids = list(map(lambda x: x[0], res))

    result = db.logs.find({'agentId': {"$in": ids}}).sort("_id", 1).limit(500)
    return result

def get_agent_alarms_report(agentId):

    result = db.alarms.find({'agentId': int(agentId)}).sort("_id", 1).limit(100)
    return result

def get_all_alarms_report(siemId):
    
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT id FROM bsep.pki.siem_agent WHERE siem_center=%s', siemId)
    res = cur.fetchall()
    conn.commit()
    cur.close()
    conn.close()

    ids = list(map(lambda x: x[0], res))

    result = db.alarms.find({'agentId': {"$in": ids}}).sort("_id", 1).limit(500)
    return result


def get_agent_alarms_report_date(agentId, req):
    result = db.alarms.find({'agentId': int(agentId), 'timestamp': {'$lt': req['lt'], '$gt': req['gt']}}).sort("_id", 1).limit(1000)
    return result


def get_agent_logs_report_date(agentId, req):
    result = db.logs.find({'agentId': int(agentId), 'timestamp': {'$lt': req['lt'], '$gt': req['gt']}}).sort("_id", 1).limit(1000)
    return result

def get_all_alarms_report_date(siemId, req):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT id FROM bsep.pki.siem_agent WHERE siem_center=%s', siemId)
    res = cur.fetchall()
    conn.commit()
    cur.close()
    conn.close()

    ids = list(map(lambda x: x[0], res))
    result = db.alarms.find({'agentId': {"$in": ids}, 'timestamp': {'$lt': req['lt'], '$gt': req['gt']}}).sort("_id", 1).limit(1000)
    return result


def get_all_logs_report_date(siemId, req):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT id FROM bsep.pki.siem_agent WHERE siem_center=%s', siemId)
    res = cur.fetchall()
    conn.commit()
    cur.close()
    conn.close()
    
    ids = list(map(lambda x: x[0], res))
    result = db.logs.find({'agentId': {"$in": ids}, 'timestamp': {'$lt': req['lt'], '$gt': req['gt']}}).sort("_id", 1).limit(1000)
    return result

client = MongoClient(port=27017)
db = client.bsep

