

import {api_pki, bearer} from '../Environment/environment';
import axios from 'axios';
let certificateService = {
    // getAll: () => {
    //     let url = `${api_pki}ca`
    //     return axios.get(url);
    // },

    // {
    //    subjectName: {
    //        country: STRING,
    //        state: STRING,
    //        locality: STRING,
    //        organization: STRING,
    //        organizationalUnit: STRING,
    //        commonName: STRING,
    //        email: STRING
    //    },
    //    duration: INT,
    //    publicKey: PEM
    //}
    add: (skid, cert) => {
        let url = `${api_pki}ca/${skid}/certificate`;
        return axios.post(url, cert, bearer());
    },
    revoke: (skid) => {
        let url = `${api_pki}certificate/${skid}/revoke`;
        return axios.get(url, bearer());
    },
    getAll: (skid) => {
        let url = `${api_pki}ca/${skid}/certificates`
        return axios.get(url, bearer());
    },
    getById: (skid) => {
        let url = `${api_pki}certificate/${skid}`;
        return axios.get(url, bearer());
    },
};

export default certificateService

