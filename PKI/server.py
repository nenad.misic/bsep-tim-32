from flask import Flask, jsonify, request
import os
import utility
import json
import certificate_service
import db_service
import json_schema_validator
from certificate_authority_bean import CertificateAuthorityBean
from certificate_bean import CertificateBean
from flask_cors import CORS
from os import path
from werkzeug.exceptions import HTTPException, BadRequest
import bcrypt
from datetime import datetime

from flask_rbac import RBAC, RoleMixin, UserMixin
import requests

CERTIFICATE_MINIMUM_DURATION = 2

def login_required(fn):
    def wrapper(*args, **kwargs):
        current_user = get_current_user()
        if current_user.username == "guest":
            return jsonify(msg='Logged users only'), 403
        else:
            return fn(*args, **kwargs)
    return wrapper
    
sso_url = 'http://localhost:5001/'
app = Flask(__name__)

app.debug = True
app.config['RBAC_USE_WHITE'] = True
app.config['SECRET_KEY'] = 'super-secret'
rbac = RBAC(app)

# cors = CORS(app, resources={r"/*": {"origins": "http://localhost:3000"}})
cors = CORS(app, resources={r"/*": {"origins": "*"}})

def get_hashed_password(plain_text_password):
    # Hash a password for the first time
    #   (Using bcrypt, the salt is saved into the hash itself)
    return bcrypt.hashpw(plain_text_password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')

def check_password(plain_text_password, hashed_password):
    # Check hashed password. Using bcrypt, the salt is saved into the hash itself
    return bcrypt.checkpw(plain_text_password.encode('utf-8'), hashed_password.encode('utf-8'))

def get_sso_identity(token):
    res = requests.post(sso_url+'decode', json={'token': token})
    if res.status_code == 200:
        user = res.json()['identity']
        user['roles'] = list(map(lambda role: roles_dict[role], user['roles'][:]))
        return user
    raise Exception(res.text)

def get_current_user():
    if request.headers.get('Authorization') is None:
        return User('guest', '...', [role_guest], None, None)
    json = get_sso_identity(request.headers.get('Authorization')[7:])
    current = User(json['username'], '...', json['roles'], json['siem_center_id'], json['ca_skid'])
    return current

rbac.set_user_loader(get_current_user)


@rbac.as_role_model
class Role(RoleMixin):
    pass


@rbac.as_user_model
class User(UserMixin):
    def __init__(self, username, password, roles, siem_center_id, ca_skid):
        self.username = username
        self.password = password
        self.roles = roles
        self.siem_center_id = siem_center_id
        self.ca_skid = ca_skid


    def to_dict(self):
        jsonable = {}
        jsonable['username'] = self.username
        jsonable['password'] = self.password
        jsonable['roles'] = list(map(lambda x: x.name, self.roles))
        jsonable['siem_center_id'] = self.siem_center_id
        jsonable['ca_skid'] = self.ca_skid
        return jsonable



@app.route('/ca')
@rbac.allow(['pki_admin'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_cas():
    current_user = get_current_user()

    permitted_cas = get_permitted_cas(current_user.ca_skid)
    return {'data': list(map(utility.ca_bean_to_dict, permitted_cas))}

@app.route('/certificate/pem/<skid>')
def get_ca_pem_by_skid(skid):
    if db_service.ca_exists_by_skid(skid):
        result = db_service.get_ca_by_skid(skid)
        return result[5]
    elif db_service.certificate_exists_by_skid(skid):
        result = db_service.get_certificate_by_skid(skid)
        return result[5]
    else:
        raise BadRequest(description="Invalid subject key identifier.")

@login_required
@app.route('/certificate/<skid>')
@rbac.allow(['pki_admin'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_ca_by_skid(skid):
    current_user = get_current_user()
    if db_service.certificate_exists_by_skid(skid):
        cert_tuple = db_service.get_certificate_by_skid(skid)
        cert = certificate_service.pem_to_cert(cert_tuple[5])
        cert_ca = CertificateBean(cert, cert_tuple[5])
        ca_skid = cert_ca.authority_key_identifier
    else:
        ca_skid = skid
    permitted = get_permitted_cas(current_user.ca_skid)
    if not has_ca_permission(ca_skid, permitted):
        raise BadRequest(description="Access denied.")

    if db_service.ca_exists_by_skid(skid):
        result = db_service.get_ca_by_skid(skid)
        return utility.ca_tuple_to_dict(result)
    elif db_service.certificate_exists_by_skid(skid):
        result = db_service.get_certificate_by_skid(skid)
        return utility.certificate_tuple_to_dict(result)
    else:
        raise BadRequest(description="Invalid subject key identifier.")




@login_required
@app.route('/ca/<skid>/certificates')
@rbac.allow(['pki_admin'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_ca_certificates(skid):
    current_user = get_current_user()
    permitted = get_permitted_cas(current_user.ca_skid)
    if not has_ca_permission(skid, permitted):
        raise BadRequest(description="Access denied.")

    if db_service.ca_exists_by_skid(skid):
        result = db_service.get_certificates_for_ca(skid)
        return {'data': list(map(utility.certificate_tuple_to_dict, result))}
    else:
        raise BadRequest(description="Invalid subject key identifier.")

# body should contain:
# {
#     subjectName: {
#         country: STRING,
#         state: STRING,
#         locality: STRING,
#         organization: STRING,
#         organizationalUnit: STRING,
#         commonName: STRING,
#         email: STRING
#     },
#     duration
# }
@login_required
@app.route('/ca/<parent_id>', methods=['POST'])
@rbac.allow(['pki_admin'], methods=['POST'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def create_ca(parent_id):
    current_user = get_current_user()
    permitted = get_permitted_cas(current_user.ca_skid)
    if not has_ca_permission(parent_id, permitted):
        raise BadRequest(description="Access denied.")

    json_schema_validator.validate_ca(request.json)
    result = db_service.get_ca_by_skid(parent_id)
    if(result == None):
        raise BadRequest(description="Invalid subject key identifier.")
    parent_cert = certificate_service.pem_to_cert(result[5])
    parent_bean = CertificateAuthorityBean(parent_cert, result[5], result[6], serial_counter=result[7])
    ca_bean = certificate_service.create_ca(request.json['subjectName'], request.json['duration'], parent_bean)
    db_service.save_ca(ca_bean)
    db_service.set_ca_serial_counter(parent_id, parent_bean.serial_counter + 1)
    return {'status': 'success'}


# body should contain:
# {
#     subjectName: {
#         country: STRING,
#         state: STRING,
#         locality: STRING,
#         organization: STRING,
#         organizationalUnit: STRING,
#         commonName: STRING,
#         email: STRING
#     },
#     duration: INT,
#     publicKey: PEM
# }
@login_required
@app.route('/ca/<ca_id>/certificate', methods=['POST'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
@rbac.allow(['pki_admin'], methods=['POST'])
def generate_certificate(ca_id):
    current_user = get_current_user()
    permitted = get_permitted_cas(current_user.ca_skid)
    if not has_ca_permission(ca_id, permitted):
        raise BadRequest(description="Access denied.")

    json_schema_validator.validate_certificate(request.json)
    result = db_service.get_ca_by_skid(ca_id)
    if(result == None):
        raise BadRequest(description="Invalid subject key identifier.")
    parent_cert = certificate_service.pem_to_cert(result[5])
    ca_bean = CertificateAuthorityBean(parent_cert, result[5], result[6], serial_counter=result[7])
    cert_bean = certificate_service.generate_certificate(request.json['subjectName'], request.json['duration'], request.json['publicKey'], ca_bean)
    db_service.save_certificate(cert_bean)
    # db_service.set_ca_serial_counter(ca_id, ca_bean.serial_counter + 1)
    # return {'status': 'success'}

    bundle = certificate_service.get_ca_chain(ca_bean)
    return jsonify({ 'cert': cert_bean.pem, 'bundle': bundle })

@login_required
@app.route('/certificate/<skid>/revoke')
@rbac.allow(['pki_admin'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def revoke_certificate(skid):
    current_user = get_current_user()
    if db_service.certificate_exists_by_skid(skid):
        cert_tuple = db_service.get_certificate_by_skid(skid)
        cert = certificate_service.pem_to_cert(cert_tuple[5])
        cert_ca = CertificateBean(cert, cert_tuple[5])
        ca_skid = cert_ca.authority_key_identifier
    else:
        ca_skid = skid
    permitted = get_permitted_cas(current_user.ca_skid)
    if not has_ca_permission(ca_skid, permitted):
        raise BadRequest(description="Access denied.")

    if db_service.certificate_exists_by_skid(skid):
        db_service.revoke_certificate(skid)
        return {'status': 'success'}
    elif db_service.ca_exists_by_skid(skid):
        db_service.revoke_ca(skid)
        return {'status': 'success'}
    else:
        raise BadRequest(description="Invalid subject key identifier.")


@app.route('/certificate/<skid>/check_revoked')
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def check_certificate_revoked(skid):
    if not db_service.certificate_exists_by_skid(skid):
        if not db_service.ca_exists_by_skid(skid):
            raise BadRequest(description="Invalid subject key identifier.")
        revoked = certificate_service.check_revoked(skid, ca=True)
        return {'revoked': revoked}
    revoked = certificate_service.check_revoked(skid)
    return {'revoked': revoked}


@app.route('/siem/<name>/revoked')
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def check_siem_center_revoked(name):
    siem = db_service.get_siem_center_by_name(name)
    if siem is None:
        raise BadRequest(description="Invalid siem center name")

    skid = siem[2]
    
    if not db_service.certificate_exists_by_skid(skid):
        if not db_service.ca_exists_by_skid(skid):
            raise BadRequest(description="Invalid subject key identifier.")
        revoked = certificate_service.check_revoked(skid, ca=True)
        return {'revoked': revoked}
    revoked = certificate_service.check_revoked(skid)
    return {'revoked': revoked}



# body
# { 'publicKey': STRING, 'passphrase': 'STRING' }
@app.route('/siem/certificate', methods=['POST'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['POST']) # password protected
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def create_siem_certificate():
    json_schema_validator.validate_siem_certificate(request.json)
    name = request.json['name']
    if not db_service.siem_center_exists_by_name(name):
        raise BadRequest(description="Invalid siem center name")

    siem_center = db_service.get_siem_center_by_name(name)

    if not check_password(request.json['passphrase'], siem_center[4]):
        raise BadRequest(description='invalid siem passphrase')

    # if ca does not exist create ca
    if siem_center[3] == None:
        parent_siem = db_service.get_siem_center_by_id(siem_center[5])
        parent_ca_tuple = db_service.get_ca_by_skid(parent_siem[3])
        parent_ca = certificate_service.pem_to_cert(parent_ca_tuple[5])
        parent_ca_bean = CertificateAuthorityBean(parent_ca, parent_ca_tuple[5], parent_ca_tuple[6])
        subject_dict = utility.subject_name_to_dict(siem_center[1])
        duration = int(max(CERTIFICATE_MINIMUM_DURATION, utility.milis_to_years(parent_ca_bean.not_after - parent_ca_bean.not_before)//2))
        siem_ca_bean = certificate_service.create_ca(subject_dict, duration, parent_ca_bean)
        db_service.save_ca(siem_ca_bean)
        db_service.update_siem_center_ca(siem_ca_bean.subject_key_identifier, name)
    else: # if ca exist, get it from db
        result = db_service.get_ca_by_skid(siem_center[3])
        siem_ca = certificate_service.pem_to_cert(result[5])
        siem_ca_bean = CertificateAuthorityBean(siem_ca, result[5], result[6])
        
        # if ca has expired, create new
        if siem_ca_bean.not_after < datetime.now().timestamp() * 1000:
            parent_siem = db_service.get_siem_center_by_id(siem_center[5])
            parent_ca_tuple = db_service.get_ca_by_skid(parent_siem[3])
            parent_ca = certificate_service.pem_to_cert(parent_ca_tuple[5])
            parent_ca_bean = CertificateAuthorityBean(parent_ca, parent_ca_tuple[5], parent_ca_tuple[6])
            subject_dict = utility.subject_name_to_dict(siem_center[1])
            duration = int(max(CERTIFICATE_MINIMUM_DURATION, utility.milis_to_years(parent_ca_bean.not_after - parent_ca_bean.not_before)//2))
            siem_ca_bean = certificate_service.create_ca(subject_dict, duration, parent_ca_bean)
            db_service.save_ca(siem_ca_bean)
            db_service.update_siem_center_ca(siem_ca_bean.subject_key_identifier, name)


    # if siem certificate does not exist, create certificate
    if siem_center[2] == None:
        siem_cert_bean = certificate_service.create_siem_certificate(siem_ca_bean, request.json['publicKey'])
        db_service.save_certificate(siem_cert_bean)
        db_service.update_siem_center_certificate(siem_cert_bean.subject_key_identifier, name)
    else: # if certificate exists, fetch
        siem_cert_tuple = db_service.get_certificate_by_skid(siem_center[2])
        siem_cert = certificate_service.pem_to_cert(siem_cert_tuple[5])
        siem_cert_bean = CertificateBean(siem_cert, siem_cert_tuple[5])

        # if certificate is expired, create new
        if siem_cert_bean.not_after < datetime.now().timestamp() * 1000:
            siem_cert_bean = certificate_service.create_siem_certificate(siem_ca_bean, request.json['publicKey'])
            db_service.save_certificate(siem_cert_bean)
            db_service.update_siem_center_certificate(siem_cert_bean.subject_key_identifier, name)


    bundle = certificate_service.get_ca_chain(siem_ca_bean)
    return jsonify({ 'cert': siem_cert_bean.pem, 'bundle': bundle })


@app.route('/agent', methods=['POST'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['POST'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def request_new_agent():
    json_schema_validator.validate_agent_request(request.json)
    name = request.json['siem_center_name']
    if not db_service.siem_center_exists_by_name(name):
        raise BadRequest(description="Invalid siem center name")
    siem_center = db_service.get_siem_center_by_name(name)
    aid = db_service.insert_agent(siem_center[0])
    return jsonify({'id': aid})

# body
# id: integer
# password: string
# subjectName: subject_dict
# publicKey: string
@app.route('/agent/certificate', methods=['POST'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['POST']) # password protected
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_agent_certificate():
    json_schema_validator.validate_get_agent_certificate(request.json)
    agent_id = request.json['id']
    password = request.json['password']
    subject_dict = request.json['subjectName']
    public_key = request.json['publicKey']
    agent = db_service.get_agent_by_id(agent_id)

    # if agent is not approved, return false
    if agent is None:
        return jsonify({'status': False})
        
    if not agent[1]:
        return jsonify({'status': False})

    # check password
    if not check_password(password, agent[4]):
        return jsonify({'status': False})

    # get siem center ca
    siem_center = db_service.get_siem_center_by_id(agent[3])
    siem_ca_tuple = db_service.get_ca_by_skid(siem_center[3])
    siem_ca = certificate_service.pem_to_cert(siem_ca_tuple[5])
    siem_ca_bean = CertificateAuthorityBean(siem_ca, siem_ca_tuple[5], siem_ca_tuple[6])
    # calculate duration in case new cert needs to be created
    duration = int(max(CERTIFICATE_MINIMUM_DURATION, utility.milis_to_years(siem_ca_bean.not_after - siem_ca_bean.not_before)//2))

    # if agent does not have certificate, create certificate
    if agent[2] == None:
        agent_cert_bean = certificate_service.generate_certificate(subject_dict, duration, public_key, siem_ca_bean)
        db_service.save_certificate(agent_cert_bean)
        db_service.update_agent_certificate(agent_cert_bean.subject_key_identifier, agent_id)
    # if certificate exists, fetch
    else:
        agent_cert_tuple = db_service.get_certificate_by_skid(agent[2])
        agent_cert = certificate_service.pem_to_cert(agent_cert_tuple[5])
        agent_cert_bean = CertificateBean(agent_cert, agent_cert_tuple[5])
        
        # if certificate has expired, create new
        if agent_cert_bean.not_after < datetime.now().timestamp() * 1000:
            agent_cert_bean = certificate_service.generate_certificate(subject_dict, duration, public_key, siem_ca_bean)
            db_service.save_certificate(agent_cert_bean)
            db_service.update_agent_certificate(agent_cert_bean.subject_key_identifier, agent_id)


    bundle = certificate_service.get_ca_chain(siem_ca_bean)

    return jsonify({'cert': agent_cert_bean.pem, 'bundle': bundle, 'status': True})



@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response


# @app.route('/protected')
# @rbac.allow(['admin'], methods=['GET'])
# @rbac.deny(['regular', 'guest'], methods=['GET'])
# def protected():
#     current_user = get_current_user()
#     return jsonify(logged_in_as=current_user.to_dict()), 200

# @app.route('/blogin')
# @login_required
# def blogin():
#     return jsonify(logged_in_as=123), 200


def get_permitted_cas(skid):
    cas = db_service.get_cas()
    root_ca = None
    permitted = []
    for ca in cas:
        if ca[2] == skid:
            root_ca = ca
            break
    if root_ca is None:
        return permitted
    permitted = filter_ca_tree(root_ca, cas)

    return list(map(ca_tuple_to_bean, permitted))

def ca_tuple_to_bean(tuple):
    cert = certificate_service.pem_to_cert(tuple[5])
    bean = CertificateAuthorityBean(cert, tuple[5], tuple[6])
    return bean


# accepts tuples
def filter_ca_tree(root_ca, all_cas):
    queue = []
    current_ca = root_ca
    result = [root_ca]
    queue.append(current_ca)
    while len(queue) > 0:
        current_ca = queue.pop(0)
        for ca in all_cas:
            if ca[3] == current_ca[2] and ca[2] != ca[3]:
                queue.append(ca)
                result.append(ca)
    return result

def has_ca_permission(skid, cas):
    return any(ca.subject_key_identifier == skid for ca in cas)



role_pki_admin = Role('pki_admin')
role_siem_admin = Role('siem_admin')
role_guest = Role('guest')

roles_dict = {
    'pki_admin': role_pki_admin,
    'siem_admin': role_siem_admin,
    'guest': role_guest
}

if __name__ == "__main__":
    root_ca = None
    if not path.exists(certificate_service.CERTIFICATE_DIRECTORY):
        os.mkdir(certificate_service.CERTIFICATE_DIRECTORY)
    # if root ca does not exist, create root ca
    if db_service.get_ca_count() == 0:
        root_ca = certificate_service.create_root()
        db_service.save_ca(root_ca)

    # if pki server certificate does not exist, create pki server certificate
    if not path.exists(certificate_service.CERTIFICATE_DIRECTORY + '/pki_cert.pem'):
        result = db_service.get_ca_by_subject_name('O=DefenceFirst, OU=PKI, CN=defencefirst, C=RS, ST=Vojvodina')
        root_ca = certificate_service.pem_to_cert(result[5])
        root_ca_bean = CertificateAuthorityBean(root_ca, result[5], result[6], no_authority_key_id=True)
        root_cert = certificate_service.create_server_cert(root_ca_bean)
        db_service.save_certificate(root_cert)
    
    # if root siem does not exist, create root siem
    if db_service.get_siem_center_count() == 0:
        result = db_service.get_ca_by_subject_name('O=DefenceFirst, OU=PKI, CN=defencefirst, C=RS, ST=Vojvodina')
        root_ca = certificate_service.pem_to_cert(result[5])
        root_ca_bean = CertificateAuthorityBean(root_ca, result[5], result[6], no_authority_key_id=True)
        siem_ca = certificate_service.create_siem_ca(root_ca_bean)
        db_service.save_ca(siem_ca)
        db_service.save_siem_center('O=DefenceFirst, OU=SIEM_CENTER, CN=defencefirst, C=RS, ST=Vojvodina', None, siem_ca.subject_key_identifier, get_hashed_password('siem'), None)



    # run server with pki certificate
    app.run(ssl_context=(certificate_service.CERTIFICATE_DIRECTORY + '/pki_cert.pem', certificate_service.CERTIFICATE_DIRECTORY + '/pki_cert.key'), debug=True)