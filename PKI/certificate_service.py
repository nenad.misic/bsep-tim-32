import OpenSSL
import utility as util
import db_service
from datetime import datetime
from certificate_authority_bean import CertificateAuthorityBean
from certificate_bean import CertificateBean

PASSPHRASE = b'B1F88652E82196CED3F9A96B20BAB58BA826BA1284A2E13ECBF54845FAF87DA34022C66CC561E59A09678F670D61D4B5'
ALT_NAME_BASE_URL = b'DNS:https://localhost:5000/certificate/pem/'
ALT_NAME = b'localhost'
CERTIFICATE_DIRECTORY = './certificates'
DOMAIN_NAME = 'localhost'

BASE_DATE = datetime(2020, 5, 15)

def create_root():
    # generate key
    key = OpenSSL.crypto.PKey()
    key.generate_key(OpenSSL.crypto.TYPE_RSA, 2048)

    # generate a self signed certificate
    cert = OpenSSL.crypto.X509()
    cert.set_version(3)
    cert.get_subject().O = 'DefenceFirst'
    cert.get_subject().OU = 'PKI'
    cert.get_subject().CN = 'defencefirst'
    cert.get_subject().C = 'RS'
    cert.get_subject().ST = 'Vojvodina'

    cert.set_serial_number(get_serial_number())
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(20*365*24*60*60)
    cert.set_issuer(cert.get_subject())
    cert.set_pubkey(key)
    

    extensions = []
    extensions.append(OpenSSL.crypto.X509Extension(b'basicConstraints', False, b'CA:TRUE'))
    extensions.append(OpenSSL.crypto.X509Extension(b'subjectKeyIdentifier', False , b'hash', subject=cert))
    cert.add_extensions(extensions)
    
    # add authorityKeyIdentifier (requires subjectKeyIdentifier)
    cert.add_extensions([OpenSSL.crypto.X509Extension(b'authorityKeyIdentifier' , False, b'keyid:always', issuer=cert)])
    # cert.add_extensions([OpenSSL.crypto.X509Extension(b'subjectAltName', False, ALT_NAME_BASE_URL + util.get_subject_key_identifier(cert).encode())])
    # cert.add_extensions([OpenSSL.crypto.X509Extension(b'subjectAltName', False, b'IP:127.0.0.1')])
    # cert.add_extensions([OpenSSL.crypto.X509Extension(b'issuerAltName', False, b'issuer:copy', issuer=cert)])

    cert.sign(key, 'sha256')

    pem = cert_to_pem(cert)
    private_key_pem = key_to_pem(key)

    save_key_and_pem_to_file(pem, key, 'rootCA')

    ca_bean = CertificateAuthorityBean(cert, pem, private_key_pem, no_authority_key_id = False)

    #server_cert = create_server_cert(ca_bean)

    return ca_bean #, server_cert

def create_server_cert(root_bean):
    # generate key
    key = OpenSSL.crypto.PKey()
    key.generate_key(OpenSSL.crypto.TYPE_RSA, 2048)

    subject_dict = {
        'organization': 'Defence First',
        'organizationalUnit': 'PKI',
        'commonName': DOMAIN_NAME,
        'country': 'RS',
        'state': 'Vojvodina'
    }

    public_key = OpenSSL.crypto.dump_publickey(OpenSSL.crypto.FILETYPE_PEM, key)
    server_cert = generate_certificate(subject_dict, 10, public_key, root_bean)
    
    save_key_and_pem_to_file(server_cert.pem, key, 'pki_cert')

    return server_cert

def create_siem_ca(root_bean):
    subject_dict = {
        'organization': 'Defence First',
        'organizationalUnit': 'SIEMcenter',
        'commonName': 'SIEMcenterCA',
        'country': 'RS',
        'state': 'Vojvodina'
    }

    siem_ca = create_ca(subject_dict, 10, root_bean)

    key = pem_to_key(siem_ca.private_key)

    save_key_and_pem_to_file(siem_ca.pem, key, 'siem_ca')
    return siem_ca

def create_siem_certificate(siem_ca_bean, public_key):
    subject_dict = {
        'organization': 'Defence First',
        'organizationalUnit': 'SIEMcenter',
        'commonName': DOMAIN_NAME,
        'country': 'RS',
        'state': 'Vojvodina'
    }

    siem_cert = generate_certificate(subject_dict, 10, public_key, siem_ca_bean)
    return siem_cert



def save_key_and_pem_to_file(pem, key, name):
    f_cert = open(CERTIFICATE_DIRECTORY + '/' + name + '.crt', 'w')
    f_cert.write(pem)
    f_cert.close()

    f_cert = open(CERTIFICATE_DIRECTORY + '/' + name + '.pem', 'w')
    f_cert.write(pem)
    f_cert.close()

    f_key = open(CERTIFICATE_DIRECTORY + '/' + name + '.key', 'w')
    f_key.write(OpenSSL.crypto.dump_privatekey(OpenSSL.crypto.FILETYPE_PEM, key).decode('ascii'))
    f_key.close()

def alter_duration(duration, issuer_bean):
    current_time = datetime.now().timestamp() * 1000
    parent_duration = util.milis_to_years(issuer_bean.not_after - current_time)
    if duration > parent_duration:
        duration = parent_duration
    elif duration < parent_duration:
        duration = parent_duration
    return duration


# generate certificate authority signed by given issuer
def create_ca(subject_dict, duration_in_years, issuer_bean):    
    # generate key

    key = OpenSSL.crypto.PKey()
    key.generate_key(OpenSSL.crypto.TYPE_RSA, 2048)

    issuer_cert = pem_to_cert(issuer_bean.pem)
    issuer_key = pem_to_key(issuer_bean.private_key)

    duration = alter_duration(duration_in_years, issuer_bean)

    cert = OpenSSL.crypto.X509()
    cert.set_version(3)
    cert = set_certificate_subject(cert, subject_dict)
    cert.set_serial_number(get_serial_number())
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(util.years_to_millis(duration_in_years))
    cert.set_issuer(issuer_cert.get_subject())
    cert.set_pubkey(key)

    extensions = []
    extensions.append(OpenSSL.crypto.X509Extension(b'basicConstraints', False, b'CA:TRUE'))
    extensions.append(OpenSSL.crypto.X509Extension(b'subjectKeyIdentifier', False , b'hash', subject=cert))
    cert.add_extensions(extensions)
    
    # add authorityKeyIdentifier (requires subjectKeyIdentifier)
    cert.add_extensions([OpenSSL.crypto.X509Extension(b'authorityKeyIdentifier' , False, b'keyid:always', issuer=issuer_cert)])
    # cert.add_extensions([OpenSSL.crypto.X509Extension(b'subjectAltName', False, ALT_NAME_BASE_URL + util.get_subject_key_identifier(cert).encode())])
    # cert.add_extensions([OpenSSL.crypto.X509Extension(b'issuerAltName', False, b'issuer:copy', issuer=issuer_cert)])

    cert.sign(issuer_key, 'sha256')

    pem = cert_to_pem(cert)
    private_key_pem = key_to_pem(key)

    ca_bean = CertificateAuthorityBean(cert, pem, private_key_pem)
    return ca_bean

# Generate certificate cigned bu given issuer
def generate_certificate(subject_dict, duration_in_years, public_key, issuer_bean):
    # generate key
    key = OpenSSL.crypto.load_publickey(OpenSSL.crypto.FILETYPE_PEM, public_key)

    issuer_cert = pem_to_cert(issuer_bean.pem)
    issuer_key = pem_to_key(issuer_bean.private_key)

    duration = alter_duration(duration_in_years, issuer_bean)

    cert = OpenSSL.crypto.X509()
    cert.set_version(3)
    cert = set_certificate_subject(cert, subject_dict)
    cert.set_serial_number(get_serial_number())
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(util.years_to_millis(duration_in_years))
    cert.set_issuer(issuer_cert.get_subject())
    cert.set_pubkey(key)

    extensions = []
    extensions.append(OpenSSL.crypto.X509Extension(b'basicConstraints', False, b'CA:FALSE'))
    extensions.append(OpenSSL.crypto.X509Extension(b'subjectKeyIdentifier', False , b'hash', subject=cert))
    cert.add_extensions(extensions)
    
    # add authorityKeyIdentifier (requires subjectKeyIdentifier)
    cert.add_extensions([OpenSSL.crypto.X509Extension(b'authorityKeyIdentifier' , False, b'keyid:always', issuer=issuer_cert)])
    # cert.add_extensions([OpenSSL.crypto.X509Extension(b'subjectAltName', False, ALT_NAME_BASE_URL + util.get_subject_key_identifier(cert).encode())])
    # cert.add_extensions([OpenSSL.crypto.X509Extension(b'issuerAltName', False, b'issuer:copy', issuer=issuer_cert)])

    cert.sign(issuer_key, 'sha256')

    pem = cert_to_pem(cert)

    cert_bean = CertificateBean(cert, pem)
    return cert_bean

# set certificate subject from subject name dictionary
def set_certificate_subject(cert, subject_dict):
    if 'organization' in subject_dict and len(subject_dict['organization']) > 0:
        cert.get_subject().O = subject_dict['organization']

    if 'organizationalUnit' in subject_dict and len(subject_dict['organizationalUnit']) > 0:
        cert.get_subject().OU = subject_dict['organizationalUnit']

    if 'commonName' in subject_dict and len(subject_dict['commonName']) > 0:
        cert.get_subject().CN = subject_dict['commonName']

    if 'locality' in subject_dict and len(subject_dict['locality']) > 0:
        cert.get_subject().L = subject_dict['locality']

    if 'state' in subject_dict and len(subject_dict['state']) > 0:
        cert.get_subject().ST = subject_dict['state']

    if 'country' in subject_dict and len(subject_dict['country']) > 0:
        cert.get_subject().C = subject_dict['country']

    if 'email' in subject_dict and len(subject_dict['email']) > 0:
        cert.get_subject().emailAddress = subject_dict['email']

    return cert

# dump certificate to text
def cert_to_text(cert):
    return OpenSSL.crypto.dump_certificate(OpenSSL.crypto.FILETYPE_TEXT, cert)

# dump certificat to pem
def cert_to_pem(cert):
    return OpenSSL.crypto.dump_certificate(OpenSSL.crypto.FILETYPE_PEM, cert).decode('ascii')

def pem_to_cert(pem):
    return OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, pem)

# dump key to text with password and cipher
def key_to_pem(key, passphrase=PASSPHRASE, cipher='AES-256-CBC'):
    return OpenSSL.crypto.dump_privatekey(OpenSSL.crypto.FILETYPE_PEM, key, cipher=cipher, passphrase=passphrase).decode('ascii')

def pem_to_key(pem, passphrase=PASSPHRASE):
    return OpenSSL.crypto.load_privatekey(OpenSSL.crypto.FILETYPE_PEM, pem, passphrase=passphrase)

def check_revoked(skid, ca=False):
    current_skid = skid
    if not ca:
        result = db_service.get_certificate_by_skid(skid)
        if result[6]:
            return True
        current_skid = result[3]
    
    while True:
        result = db_service.get_ca_by_skid(current_skid)
        if result[10]:
            return True

        if result[2] == result[3]:
            return False
        
        current_skid = result[3]

def get_ca_chain(ca_bean):
    current_ca_bean = ca_bean
    text = ca_bean.pem
    while(current_ca_bean.subject_key_identifier != current_ca_bean.authority_key_identifier):
        result = db_service.get_ca_by_skid(current_ca_bean.authority_key_identifier)
        current_ca = pem_to_cert(result[5])
        current_ca_bean = CertificateAuthorityBean(current_ca, result[5], result[6])
        text += '\n' + current_ca_bean.pem
    return text


# returns time elapsed since BASE_DATE in microseconds
def get_serial_number():
    d = datetime.now() - BASE_DATE
    return d.days*86400000000 + d.seconds*1000000 + d.microseconds

if __name__ == '__main__':
    for i in range(100):
        key = OpenSSL.crypto.PKey()
        key.generate_key(OpenSSL.crypto.TYPE_RSA, 2048)
        print(OpenSSL.crypto.dump_publickey(OpenSSL.crypto.FILETYPE_PEM, key))
        
    # text = OpenSSL.crypto.dump_privatekey(OpenSSL.crypto.FILETYPE_TEXT, key)
    # print(text)
    # print(pem)
    # print(text==OpenSSL.crypto.dump_privatekey(OpenSSL.crypto.FILETYPE_TEXT, pem_to_key(pem)))
