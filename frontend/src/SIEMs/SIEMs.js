import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import SortableTree from 'react-sortable-tree';
import ReactSortableTreeThemeFileExplorer from 'react-sortable-tree-theme-full-node-drag';
import siemService from '../Services/siemService';
import agentService from '../Services/agentService';
import { Redirect } from 'react-router-dom'

var axios = require('axios')
var https = require('https')
var fs = require('fs')

class SIEMs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detail: {},
      siems: [],
    };
  }

  async componentDidMount() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('siem_admin'))
    {
      return;
    }
    var raw_data = (await siemService.getAll()).data.data;
    raw_data = raw_data.map(e => {e.title = e.name.split(',')[0].split('=')[1]; return e;});
    var data_tree = [siemService.buildTree(raw_data)];
    this.setState({
      detail: undefined,
      siems: data_tree
    })  
  }

  render() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('siem_admin'))
    {
      return <Redirect to='/unauthorized' />
    }
    return (
      
      <div className="container-fluid">
        <div className="row">
          <div className= "col-md-4 col-12 card" style={{ height: '84vh' }}>
            <SortableTree
              canDrag={false}
              treeData={this.state.siems}
              onChange={siems => this.setState({ siems })}
              theme={ReactSortableTreeThemeFileExplorer}
              generateNodeProps={({ node, path }) => ({
                buttons: [
                  <button className="btn btn-primary"
                    onClick={async () => {
                      let agents = await agentService.getAll(node.id);
                      let detailedSiemData = await siemService.getById(node.id);
                      let detailedNode = detailedSiemData.data;
                      detailedNode.agents = agents.data.data;
                      this.setState({
                        detail: detailedNode,
                      })
                    }
                  }
                  >
                    Details
                  </button>,
                ],
              })}
            />
          </div>

          {this.state.detail && this.state.detail.name &&  
          <div className= "col-md-4 pr-0 col-12">
              <div className="pb-5 card col-12" style={{ height: 'fit-content' }}>
                <div className="row">
                  <div className="col-6">
                  {this.state.detail.name.split('emailAddress=')[0].split`,`.map(e => (
                <h2 className="my-2">{e}</h2>
                ))}
                
                {/* <p>{new Date(this.state.detail.not_before).toLocaleDateString()} - {new Date(this.state.detail.not_after).toLocaleDateString()}</p> */}
                <Link to={`/siem/${this.state.detail.id}`} className="card-link text-info">Details</Link>
                  </div>
                  {/* <div  className="col-5 offset-1 my-auto " >
                    {this.state.detail.revoked && 
                      <img src="/revoked_stamp.svg" style={{width: '100%', height:'auto', 'transform': `rotate(13deg)`}}></img>
                    }
                  </div> */}

                </div>
                
                <hr className="my-4"></hr>
                {/* <div className="my-4"><h3 className="d-inline">Issued to: </h3><p className="d-inline">{this.state.detail.subject}</p></div> */}
                {/* <div className="my-4"><h3 className="d-inline">Issued by: </h3><p className="d-inline">{this.state.detail.issuer}</p></div> */}
                {/* <div className="my-4"><h3 className="d-inline">Valid from: </h3><p className="d-inline">{new Date(this.state.detail.not_before).toLocaleDateString()}</p></div>
                <div className="my-4"><h3 className="d-inline">Valid to: </h3><p className="d-inline">{new Date(this.state.detail.not_after).toLocaleDateString()}</p></div> */}
                {/* <hr className="my-4"></hr> */}
                <h3 className="my-4">Name: </h3><p>{this.state.detail.name}</p>
                <br className="mb-5"></br>
                <div className="container">
                  <div className="row">
                    <div className="btn-group blocks col-md-12" >
                      <Link className="offset-md-1 col-md-6 btn btn-success" to={`/add-siem/${this.state.detail.id}`}>
                        Add new siem center
                      </Link>
                      <Link className="col-md-6 btn btn-info" to={`/rule`}>
                        Add new rule
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
              <div>
              </div>
          </div> 
          }
          {this.state.detail && this.state.detail.agents && this.state.detail.agents.length !== 0 &&
            <div className= "col-md-4 col-12">
            <div className="pb-5 col-12" style={{ height: 'fit-content' }}>
                    <h2>Agents:</h2>
                  {this.state.detail.agents.map(e => (<div className={`border radius-custom mb-3 row card-border`}>
                    <div className="col-9">
                      <h5 className="card-title">{e.name}</h5>
                      <h6 className="card-subtitle mb-2 text-muted">ID: {e.id}</h6>
                      </div>
                      <div  className="col-3 my-auto " >
                  </div>
                  </div>
                    ))
                    }
                    </div>
                    
            </div>
            }

        </div>
        </div>
        
    );
  }
}

export default SIEMs;