import psycopg2

from User import User

# run db_service.py to create schema

SYS_ADMIN_CONNECT_OPTIONS = "dbname='bsep' user='postgres' host='localhost' password='postgres'"
PKI_ADMIN_CONNECT_OPTIONS = "dbname='bsep' user='pki_admin' host='localhost' password='pki_admin'"


def save_user(user_bean):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()


    cur.execute('INSERT INTO bsep.pki.user (username, \
        password) \
        VALUES (%s, %s)',
        (user_bean.username, user_bean.password))

    for role in user_bean.roles:
        cur.execute('INSERT INTO bsep.pki.user_user_role (user_id, \
            role_id) \
            VALUES (%s, %s)',
            (user_bean.username, role))

    conn.commit()
    cur.close()
    conn.close()

def get_user_by_username(username):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.user WHERE username=%s', (username,))
    result = cur.fetchone()
    if result is None:
        return None
    username = result[0]
    password = result[1]
    siem_center_id = result[2]
    ca_skid = result[3]
    cur.execute('SELECT * FROM bsep.pki.user_user_role WHERE user_id=%s', (username,))
    roles = list(map(lambda x: x[2], cur.fetchall()))
    conn.commit()
    cur.close()
    conn.close()
    return User(username, password, roles, siem_center_id, ca_skid)