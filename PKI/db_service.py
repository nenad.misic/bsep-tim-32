import psycopg2

# run db_service.py to create schema

SYS_ADMIN_CONNECT_OPTIONS = "dbname='bsep' user='postgres' host='localhost' password='postgres'"
PKI_ADMIN_CONNECT_OPTIONS = "dbname='bsep' user='pki_admin' host='localhost' password='pki_admin'"


    
def create_schema():
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()

    create_schema_sql = open('./../SQL/INSERT.sql', 'r')
    cur.execute(create_schema_sql.read())
    create_schema_sql.close()

    conn.commit()
    cur.close()
    conn.close()

def get_cas():
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.certificate_authority')
    result = cur.fetchall()
    conn.commit()
    cur.close()
    conn.close()
    return result


def save_ca(ca_bean):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()


    cur.execute('INSERT INTO bsep.pki.certificate_authority (subject, \
        issuer, subject_key_identifier, authority_key_identifier, serial_number, pem, private_key, not_before, not_after) \
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)',
        (ca_bean.subject, ca_bean.issuer, ca_bean.subject_key_identifier, ca_bean.authority_key_identifier, ca_bean.serial_number,
        ca_bean.pem, ca_bean.private_key, ca_bean.not_before, ca_bean.not_after))


    conn.commit()
    cur.close()
    conn.close()

def get_ca_count():
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT COUNT(*) FROM bsep.pki.certificate_authority')
    result = cur.fetchone()
    conn.commit()
    cur.close()
    conn.close()
    return result[0]

# get certificate authority by subject key identifier
def get_ca_by_skid(skid):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.certificate_authority WHERE subject_key_identifier=%s', (skid,))
    result = cur.fetchone()
    conn.commit()
    cur.close()
    conn.close()
    return result

def get_ca_by_subject_name(name):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.certificate_authority WHERE subject=%s', (name,))
    result = cur.fetchone()
    conn.commit()
    cur.close()
    conn.close()
    return result


def ca_exists_by_skid(skid):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT COUNT(*) FROM bsep.pki.certificate_authority WHERE subject_key_identifier=%s', (skid,))
    result = cur.fetchone()
    conn.commit()
    cur.close()
    conn.close()
    return result[0] > 0

def revoke_ca(skid):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute("UPDATE bsep.pki.certificate_authority SET revoked='true' WHERE subject_key_identifier=%s", (skid,))
    conn.commit()
    cur.close()
    conn.close()


def set_ca_serial_counter(skid, counter):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('UPDATE bsep.pki.certificate_authority SET serial_counter=%s WHERE subject_key_identifier=%s', (counter, skid))
    conn.commit()
    cur.close()
    conn.close()

def get_certificates_for_ca(skid):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.certificate WHERE authority_key_identifier=%s', (skid,))
    result = cur.fetchall()
    conn.commit()
    cur.close()
    conn.close()
    return result


def save_certificate(certificate_bean):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()


    cur.execute('INSERT INTO bsep.pki.certificate (subject, \
        issuer, subject_key_identifier, authority_key_identifier, serial_number, pem, not_before, not_after) \
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)',
        (certificate_bean.subject, certificate_bean.issuer, certificate_bean.subject_key_identifier, certificate_bean.authority_key_identifier,
        certificate_bean.serial_number, certificate_bean.pem, certificate_bean.not_before, certificate_bean.not_after))


    conn.commit()
    cur.close()
    conn.close()

# get certificate authority by subject key identifier
def get_certificate_by_skid(skid):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.certificate WHERE subject_key_identifier=%s', (skid,))
    result = cur.fetchone()
    conn.commit()
    cur.close()
    conn.close()
    return result

def certificate_exists_by_skid(skid):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT COUNT(*) FROM bsep.pki.certificate WHERE subject_key_identifier=%s', (skid,))
    result = cur.fetchone()
    conn.commit()
    cur.close()
    conn.close()
    return result[0] > 0


def revoke_certificate(skid):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute("UPDATE bsep.pki.certificate SET revoked='true' WHERE subject_key_identifier=%s", (skid,))
    conn.commit()
    cur.close()
    conn.close()

def save_siem_center(name, certificate, certificate_authority, password, parent_siem):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()

    cur.execute('INSERT INTO bsep.pki.siem_center (name, \
            certificate, certificate_authority, password, parent_siem) \
            VALUES (%s, %s, %s, %s, %s)',
                (name, certificate, certificate_authority, password, parent_siem))

    conn.commit()
    cur.close()
    conn.close()

def siem_center_exists_by_name(name):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT COUNT(*) FROM bsep.pki.siem_center WHERE name=%s', (name,))
    result = cur.fetchone()
    conn.commit()
    cur.close()
    conn.close()
    return result[0] > 0

def get_siem_center_by_name(name):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.siem_center WHERE name=%s', (name,))
    result = cur.fetchone()
    conn.commit()
    cur.close()
    conn.close()
    return result

def update_siem_center_certificate(cert_skid, name):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('UPDATE bsep.pki.siem_center SET certificate=%s WHERE name=%s', (cert_skid, name))
    conn.commit()
    cur.close()
    conn.close()

if __name__ == '__main__':
    create_schema()


def get_siem_center_by_id(id):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.siem_center WHERE id=%s', (id,))
    result = cur.fetchone()
    conn.commit()
    cur.close()
    conn.close()
    return result


def update_siem_center_ca(skid, name):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('UPDATE bsep.pki.siem_center SET certificate_authority=%s WHERE name=%s', (skid, name))
    conn.commit()
    cur.close()
    conn.close()

def get_siem_center_count():
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    result = cur.execute('SELECT COUNT(*) FROM bsep.pki.siem_center')
    result = cur.fetchone()
    conn.commit()
    cur.close()
    conn.close()
    return result[0]

def insert_agent(siem_center_id):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()

    cur.execute('INSERT INTO bsep.pki.siem_agent (siem_center) \
                VALUES (%s) RETURNING id',
                (siem_center_id,))
    result = cur.fetchone()[0]

    conn.commit()
    cur.close()
    conn.close()
    return result


def get_agent_by_id(aid):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('SELECT * FROM bsep.pki.siem_agent WHERE id=%s', (aid,))
    result = cur.fetchone()
    conn.commit()
    cur.close()
    conn.close()
    return result


def update_agent_certificate(skid, aid):
    conn = psycopg2.connect(SYS_ADMIN_CONNECT_OPTIONS)
    cur = conn.cursor()
    cur.execute('UPDATE bsep.pki.siem_agent SET certificate=%s WHERE id=%s', (skid, aid))
    conn.commit()
    cur.close()
    conn.close()