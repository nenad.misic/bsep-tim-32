

def watch_windows_logs():
    import win32evtlog
    import win32event

    logtype = 'Application'
    server = 'localhost'

    DELAY = 2


    current = 0


    hand=win32evtlog.OpenEventLog(server,logtype)
    total=win32evtlog.GetNumberOfEventLogRecords(hand)
    flags = win32evtlog.EVENTLOG_BACKWARDS_READ|win32evtlog.EVENTLOG_SEQUENTIAL_READ
    events=win32evtlog.ReadEventLog(hand,flags,0)

    agent_id = 1
                    # log = LogFromAgent(timestamp.isoformat(), str(event.SourceName), socket.gethostbyname(socket.gethostname()), str(event.EventCategory), str(event.EventType), "", "", "System", "Windows")

    while True:
        total=win32evtlog.GetNumberOfEventLogRecords(hand)
        if total == current:
            time.sleep(DELAY)
            continue

        events = win32evtlog.ReadEventLog(hand, flags1,0)

        for ev_obj in events:
            the_time=ev_obj.TimeGenerated.Format() #'12/23/99 15:54:09'
            computer=str(ev_obj.ComputerName)
            cat=ev_obj.EventCategory
            seconds=date2sec(the_time)
            record=ev_obj.RecordNumber
            msg=str(win32evtlogutil.SafeFormatMessage(ev_obj, logtype))
            source=str(ev_obj.SourceName)
            print(the_time, evt_id, computer, cat, seconds, record, msg, source)

            log_dict = {
                "agentId": agent_id,
                "level": 'INFO',
                "timestamp": seconds,
                "source": source,
                "eventId": evt_id,
                "taskCategory": cat,
                "message": msg
            }

            current += 1

            if not BATCH_MODE:
                post_body = { 'logs': [log_dict ]}
                response = requests.post(SIEM_CENTER_URL+'log', json=post_body, cert=cert, verify=CERTIFICATE_DIRECTORY + 'bundle.crt')
                print('log sent', response.json())
            else:
                logs_batch.append(log_dict)

