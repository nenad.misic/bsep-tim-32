class User:
    def __init__(self, username, password, roles, siem_center_id, ca_skid):
        self.username = username
        self.password = password
        self.roles = roles
        self.siem_center_id = siem_center_id
        self.ca_skid = ca_skid

    def to_dict(self):
        jsonable = {}
        jsonable['username'] = self.username
        jsonable['password'] = self.password
        jsonable['roles'] = self.roles
        jsonable['siem_center_id'] = self.siem_center_id
        jsonable['ca_skid'] = self.ca_skid
        return jsonable