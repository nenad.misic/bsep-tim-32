from flask import Flask, jsonify, request
import bcrypt
import os
import json
from flask_cors import CORS
from os import path
from werkzeug.exceptions import HTTPException, BadRequest
import requests
import utility
import db_service
from bson.json_util import dumps
from flask_rbac import RBAC, RoleMixin, UserMixin
import json_schema_validator

def login_required(fn):
    def wrapper(*args, **kwargs):
        current_user = get_current_user()
        if current_user.username == "guest":
            return jsonify(msg='Logged users only'), 403
        else:
            return fn(*args, **kwargs)
    return wrapper

CERTIFICATE_DIRECTORY = './certificates'
sso_url = 'http://localhost:5001/'

app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'super-secret'
app.config['RBAC_USE_WHITE'] = True
rbac = RBAC(app)
cors = CORS(app, resources={r"/*": {"origins": "*"}})
ALARMS_PATH = 'alarms.json'


def get_hashed_password(plain_text_password):
    # Hash a password for the first time
    #   (Using bcrypt, the salt is saved into the hash itself)
    return bcrypt.hashpw(plain_text_password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')

def check_password(plain_text_password, hashed_password):
    # Check hashed password. Using bcrypt, the salt is saved into the hash itself
    return bcrypt.checkpw(plain_text_password.encode('utf-8'), hashed_password.encode('utf-8'))


def get_sso_identity(token):
    res = requests.post(sso_url+'decode', json={'token': token})
    if res.status_code == 200:
        user = res.json()['identity']
        user['roles'] = list(map(lambda role: roles_dict[role], user['roles'][:]))
        return user
    raise Exception(res.text)


def get_current_user():
    if request.headers.get('Authorization') is None:
        return User('guest', '...', [role_guest], None, None)
    json = get_sso_identity(request.headers.get('Authorization')[7:])
    current = User(json['username'], '...', json['roles'], json['siem_center_id'], json['ca_skid'])
    return current

rbac.set_user_loader(get_current_user)


@rbac.as_role_model
class Role(RoleMixin):
    pass


@rbac.as_user_model
class User(UserMixin):
    def __init__(self, username, password, roles, siem_center_id, ca_skid):
        self.username = username
        self.password = password
        self.roles = roles
        self.siem_center_id = siem_center_id
        self.ca_skid = ca_skid


    def to_dict(self):
        jsonable = {}
        jsonable['username'] = self.username
        jsonable['password'] = self.password
        jsonable['roles'] = list(map(lambda x: x.name, self.roles))
        jsonable['siem_center_id'] = self.siem_center_id
        jsonable['ca_skid'] = self.ca_skid
        return jsonable



# - get all siem centers
@app.route('/siem', methods=['GET'])
@rbac.allow(['siem_admin'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_siem_centers():
    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    return {'data':  list(map(lambda e: utility.siem_centre_tuple_to_dict(e), db_service.get_siem_centers()))}

# - get siem center by id
@app.route('/siem/<identifier>', methods=['GET'])
@rbac.allow(['siem_admin'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_siem_center(identifier):

    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    if not has_siem_permission(identifier, permitted):
        raise BadRequest(description="Access denied.")

    return utility.siem_centre_tuple_to_dict(db_service.get_siem_center(identifier))


# - add siem center
#     - name
#     - parent
#     - password
@app.route('/siem', methods=['POST'])
@rbac.allow(['siem_admin'], methods=['POST'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def add_siem_center():
    json_schema_validator.validate_add_siem_center(request.json)
    name = request.json['name']
    password = get_hashed_password(request.json['password'])
    parent = request.json['parent']

    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    if not has_siem_permission(parent, permitted):
        raise BadRequest(description="Access denied.")

    db_service.add_siem_center(name, password, parent)

    return {'status': 'success'}


# - get agent requests
#     - siem_center_id
#     - vracas WHERE approved=false
@app.route('/agent/<siem_center_id>', methods=['GET'])
@rbac.allow(['siem_admin'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_agents(siem_center_id):
    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    if not has_siem_permission(siem_center_id, permitted):
        raise BadRequest(description="Access denied.")
    return {'data':  list(map(lambda e: utility.siem_agent_tuple_to_dict(e), db_service.get_agents(siem_center_id)))}


# - approve agent request
#     - id
#     - password
#     - SET approved=true
#     - SET password=password
@app.route('/agent', methods=['POST'])
@rbac.allow(['siem_admin'], methods=['POST'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def approve_agent():
    identification = request.json['id']
    password = get_hashed_password(request.json['password'])
    if identification is None or password is None:
        raise BadRequest(description="Access denied.")
    
    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    if not has_agent_permission(identification, permitted):
        raise BadRequest(description="Access denied.")

    db_service.approve_agent(identification, password)
    return {'status': 'success'}

@app.route('/unapproved/<siemId>')
@rbac.allow(['siem_admin'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_unapproved(siemId):
    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    if not has_siem_permission(siemId, permitted):
        raise BadRequest(description="Access denied.")

    unapproved = db_service.get_unapproved_agents(siemId)
    return { 'data': list(map(utility.siem_agent_tuple_to_dict, unapproved)) }





@app.route('/rule', methods=['POST'])
@rbac.allow(['siem_admin'], methods=['POST'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def add_rule():
    json_schema_validator.validate_rule(request.json)
    with open(ALARMS_PATH, 'r') as f:
        alarm_descriptions = json.loads(f.read())
    print(request.json)
    alarm_descriptions.append(request.json)
    with open(ALARMS_PATH, 'w') as f:
        f.write(json.dumps(alarm_descriptions, indent=4))
    return "success"
    


    
@app.route('/log/<agent>', methods=['GET'])
@rbac.allow(['siem_admin'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_all_logs_for_agent(agent):
    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    if not has_agent_permission(agent, permitted):
        raise BadRequest(description="Access denied.")
    return {'data':  dumps(db_service.get_agent_logs_report(agent))}


@app.route('/alarm/<agent>', methods=['GET'])
@rbac.allow(['siem_admin'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_all_alarms_for_agent(agent):
    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    if not has_agent_permission(agent, permitted):
        raise BadRequest(description="Access denied.")
    return {'data':  dumps(db_service.get_agent_alarms_report(agent))}


@app.route('/logDate/<agent>', methods=['POST'])
@rbac.allow(['siem_admin'], methods=['POST'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_all_logs_for_agent_in_date(agent):
    json_schema_validator.validate_lt_gt(request.json)
    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    if not has_agent_permission(agent, permitted):
        raise BadRequest(description="Access denied.")
    return {'data':  dumps(db_service.get_agent_logs_report_date(agent, request.json))}

@app.route('/alarmDate/<agent>', methods=['POST'])
@rbac.allow(['siem_admin'], methods=['POST'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_all_alarms_for_agent_in_date(agent):
    json_schema_validator.validate_lt_gt(request.json)
    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    if not has_agent_permission(agent, permitted):
        raise BadRequest(description="Access denied.")
    return {'data':  dumps(db_service.get_agent_alarms_report_date(agent, request.json))}

@app.route('/logs/<siem>', methods=['GET'])
@rbac.allow(['siem_admin'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_all_logs(siem):
    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    if not has_siem_permission(siem, permitted):
        raise BadRequest(description="Access denied.")
    return {'data':  dumps(db_service.get_all_logs_report(siem))}

@app.route('/alarms/<siem>', methods=['GET'])
@rbac.allow(['siem_admin'], methods=['GET'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_all_alarms(siem):
    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    if not has_siem_permission(siem, permitted):
        raise BadRequest(description="Access denied.")
    return {'data':  dumps(db_service.get_all_alarms_report(siem))}

@app.route('/logsDate/<siem>', methods=['POST'])
@rbac.allow(['siem_admin'], methods=['POST'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_all_logs_date(siem):
    json_schema_validator.validate_lt_gt(request.json)
    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    if not has_siem_permission(siem, permitted):
        raise BadRequest(description="Access denied.")
    return {'data':  dumps(db_service.get_all_logs_report_date(siem, request.json))}

@app.route('/alarmsDate/<siem>', methods=['POST'])
@rbac.allow(['siem_admin'], methods=['POST'])
@rbac.allow(['pki_admin', 'siem_admin', 'guest'], methods=['OPTIONS'])
def get_all_alarms_date(siem):
    json_schema_validator.validate_lt_gt(request.json)
    current_user = get_current_user()
    permitted = get_permitted_siems(current_user.siem_center_id)
    if not has_siem_permission(siem, permitted):
        raise BadRequest(description="Access denied.")
    return {'data':  dumps(db_service.get_all_alarms_report_date(siem, request.json))}
    

def get_permitted_siems(sid):
    siems = db_service.get_siem_centers()
    root_siem = None
    permitted = []
    for siem in siems:
        if siem[0] == int(sid):
            root_siem = siem
            break
    if root_siem is None:
        return permitted
    permitted = filter_siem_tree(root_siem, siems)

    return permitted


def filter_siem_tree(root_siem, siems):
    queue = []
    current_siem = root_siem
    result = [root_siem]
    queue.append(current_siem)
    while len(queue) > 0:
        current_siem = queue.pop(0)
        for siem in siems:
            if siem[5] == current_siem[0]:
                queue.append(siem)
                result.append(siem)
    return result

def has_siem_permission(sid, siems):
    return any(siem[0] == int(sid) for siem in siems)

def has_agent_permission(agentId, siems):
    agent = db_service.get_agent_by_id(int(agentId))
    siemId = agent[3]
    return has_siem_permission(siemId, siems)


@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response


role_pki_admin = Role('pki_admin')
role_siem_admin = Role('siem_admin')
role_guest = Role('guest')

roles_dict = {
    'pki_admin': role_pki_admin,
    'siem_admin': role_siem_admin,
    'guest': role_guest
}


if __name__ == "__main__":
    app.run(port=5012, ssl_context=(CERTIFICATE_DIRECTORY + '/siem_cert.crt', CERTIFICATE_DIRECTORY + '/siem_cert.key'), debug=True)
    