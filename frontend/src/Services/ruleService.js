import {api_siem, bearer} from '../Environment/environment';
import axios from 'axios';
let ruleService = {
    addRule: (rule) => {
        let url = `${api_siem}rule`;
        return axios.post(url, rule, bearer());
    },
};

export default ruleService

