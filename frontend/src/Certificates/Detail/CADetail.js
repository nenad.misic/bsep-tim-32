import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import caService from '../../Services/caService';
import certificateService from '../../Services/certificateService';
import { Redirect } from 'react-router-dom'

class CADetail extends Component {
  constructor(props) {
    super(props);
    this.subjectKeyIdentifier = '';
    this.state = {
      detail: {},
    };
  }

  async componentDidMount() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('pki_admin'))
    {
      return;
    }
    const { match: { params } } = this.props;
    this.subjectKeyIdentifier = params.subjectKeyIdentifier;
    let certificates = await certificateService.getAll(this.subjectKeyIdentifier);
    let detailedNodeData = await caService.getById(this.subjectKeyIdentifier);
    let detailedNode = detailedNodeData.data;
    detailedNode.certificates = certificates.data.data;
   
    this.setState({
      detail: detailedNode,
    })  
  }

  revokeCa = async (e) => {
    await caService.revoke(this.state.detail.subjectKeyIdentifier);
    let certificates = await certificateService.getAll(this.subjectKeyIdentifier);
    let detailedNodeData = await caService.getById(this.subjectKeyIdentifier);
    let detailedNode = detailedNodeData.data;
    detailedNode.certificates = certificates.data.data;
   
    this.setState({
      detail: detailedNode,
    })  
  }

  revokeCert = async (subjectKeyIdentifier) => {
    await certificateService.revoke(subjectKeyIdentifier);
    let certificates = await certificateService.getAll(this.state.detail.subjectKeyIdentifier);
    let detailedNode = this.state.detail;
    detailedNode.certificates = certificates.data.data;
    this.setState({
      detail: detailedNode,
    })
  }

  render() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('pki_admin'))
    {
      return <Redirect to='/unauthorized' />
    }
    return (
      
      <div className="container-fluid">
        <div className="row">
          {this.state.detail && this.state.detail.subject &&  
          <div className={`${this.state.detail.certificates.length !== 0 ? "col-md-6" : "col-md-10 offset-md-1"} pr-0 col-12`}>
              <div className="pb-5 card col-12" style={{ height: 'fit-content' }}>
                <div className="row">
                  <div className="col-6">
                  {this.state.detail.subject.split('emailAddress=')[0].split`,`.map(e => (
                <h2 className="my-2">{e}</h2>
                ))}
                <p>{new Date(this.state.detail.not_before).toLocaleDateString()} - {new Date(this.state.detail.not_after).toLocaleDateString()}</p>
                  </div>
                  <div  className="col-4 offset-1 my-auto " >
                    {this.state.detail.revoked && 
                      <img src="/revoked_stamp.svg" style={{width: '100%', height:'auto', 'transform': `rotate(13deg)`}}></img>
                    }
                  </div>

                </div>
                
                <hr className="my-4"></hr>
                <div className="my-4"><h3 className="d-inline">Issued to: </h3><p className="d-inline">{this.state.detail.subject}</p></div>
                <div className="my-4"><h3 className="d-inline">Issued by: </h3><p className="d-inline">{this.state.detail.issuer}</p></div>
                <div className="my-4"><h3 className="d-inline">Valid from: </h3><p className="d-inline">{new Date(this.state.detail.not_before).toLocaleDateString()}</p></div>
                <div className="my-4"><h3 className="d-inline">Valid to: </h3><p className="d-inline">{new Date(this.state.detail.not_after).toLocaleDateString()}</p></div>
                <hr className="my-4"></hr>
                <h3 className="my-4">Subject key identifier: </h3><p>{this.state.detail.subjectKeyIdentifier}</p>
                <br className="mb-5"></br>
                <div className="container">
                  <div className="row">
                    <div className="btn-group blocks col-md-12" >
                      <Link className="col-4 btn btn-success" to={`/add-ca/${this.state.detail.subjectKeyIdentifier}`}>
                        Add CA
                      </Link>
                      <Link className="col-4 btn btn-success" to={`/add-cert/${this.state.detail.subjectKeyIdentifier}`}>
                        Add Certificate
                      </Link>
                      <button className="col-4 btn btn-danger" onClick={this.revokeCa} disabled={this.state.detail.revoked}>Revoke</button>
                    </div>
                  </div>
                </div>
              </div>
              <div>
              </div>
          </div> 
          }
          {this.state.detail && this.state.detail.certificates && this.state.detail.certificates.length !== 0 &&
            <div className= "col-md-6 col-12">
            <div className="pb-5 col-12" style={{ height: 'fit-content' }}>
                    <h2>Certificates:</h2>
                  {this.state.detail.certificates.map(e => (<div className={`border radius-custom mb-3 row ${e.revoked ? 'border-danger' : 'card-border'}`}>
                    <div className="col-9">
                      <h5 className="card-title">{e.subject.split(',')[0].split('=')[1]}</h5>
                      <h6 className="card-subtitle mb-2 text-muted">{new Date(e.not_before).toLocaleDateString()} - {new Date(e.not_after).toLocaleDateString()}</h6>
                      <Link to={`/cert/${e.subjectKeyIdentifier}`} className="card-link text-info">Details</Link>
                      {!e.revoked && <a href="#" className="card-link text-danger" onClick={ () => this.revokeCert(e.subjectKeyIdentifier)}>Revoke</a>}
                      </div>
                      <div  className="col-2 my-auto " >
                    {e.revoked && 
                      <img src="/revoked_stamp.svg" style={{width: '100%', height:'auto'}}></img>
                    }
                  </div>
                  </div>
                    ))
                    }
                    </div>
                    
            </div>
            }
        </div>
        </div>
        
    );
  }
}

export default CADetail;