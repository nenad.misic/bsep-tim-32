import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import userService from '../Services/userService';

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refresh: 0
    };
  }

  async componentDidMount() {
    userService.onChange = (v) => this.setState({refresh: v});
  }

  render() {
    return (
      <nav className="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
        <Link className="navbar-brand" to="/">
          DefenceFirst
        </Link>
  
        <button className="navbar-toggler border" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
  
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav">
            
          {localStorage.getItem('user-token') && localStorage.getItem('identity') && JSON.parse(localStorage.getItem('identity')).roles.includes('pki_admin') && (
            <li className="nav-item">
              <Link className="nav-link px-4" to="/pki">
                PKI
              </Link>
            </li>
          )}
            {localStorage.getItem('user-token') && localStorage.getItem('identity') && JSON.parse(localStorage.getItem('identity')).roles.includes('siem_admin') && (
            <li className="nav-item">
              <Link className="nav-link px-4" to="/siems">
                SIEM
              </Link>
            </li>
            )}
            {localStorage.getItem('user-token') && localStorage.getItem('identity') && JSON.parse(localStorage.getItem('identity')).roles.includes('siem_admin') && (
            <li className="nav-item">
              <Link className="nav-link px-4" to="/agents">
                Agents
              </Link>
            </li>
            )}
            {localStorage.getItem('user-token') && localStorage.getItem('identity') && JSON.parse(localStorage.getItem('identity')).roles.includes('siem_admin') && (
            <li className="nav-item">
              <Link className="nav-link px-4" to="/unapproved">
                Unapproved Agents
              </Link>
            </li>
            )}
            {!localStorage.getItem('user-token') && (
              <li className="nav-item">
                <Link className="nav-link px-4" to="/login">
                  Login
                </Link>
              </li>
            )}
            {/* {!localStorage.getItem('user-token') && (
              <li className="nav-item">
                <Link className="nav-link px-4" to="/register">
                  Register
                </Link>
              </li>
            )} */}
            {localStorage.getItem('user-token') && (
              <li className="nav-item">
                <a href="#" className="nav-link px-4" onClick={() => {userService.logout(); this.setState({refresh: this.state.refresh + 1})}}>
                  Logout
                </a>
              </li>
            )}
          </ul>
        </div>
      </nav>
    );
  }
  
}

export default NavBar;