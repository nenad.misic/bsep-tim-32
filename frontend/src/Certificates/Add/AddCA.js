import React, {Component} from 'react';
import caService from '../../Services/caService';
import { Redirect } from 'react-router-dom'

class AddCA extends Component {
  constructor(props) {
    super(props);
    this.subjectKeyIdentifier = '';
    this.state = {
      organization: '',
      organizationalUnit: '',
      commonName: '',
      country: '',
      state: '',
      locality: '',
      duration_in_years: 0,
      val_organization: true,
      val_organizationalUnit: true,
      val_commonName: true,
      val_country: true,
      val_state: true,
      val_locality: true,
      val_duration_in_years: true,
      regex_organization: (real) => new RegExp('.+').test(real),
      regex_organizationalUnit: (real) => true,
      regex_commonName: (real) => true,
      regex_country: (real) => new RegExp('.+').test(real) && real.length == 2,
      regex_state: (real) => true,
      regex_locality: (real) => true,
      regex_duration_in_years: (real) => new RegExp('[0-9]+').test(real) && real < 20 && real > 0,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.input = React.createRef();
  }
  
  async componentDidMount() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('pki_admin'))
    {
      return;
    }
    const { match: { params } } = this.props;
    this.subjectKeyIdentifier = params.subjectKeyIdentifier;
  }

  handleChange = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val, [`val_${nam}`]: this.state[`regex_${nam}`](val)});
  }

  handleSubmit = async (event) => {
    // {
    //     subjectName: {
    //     country: STRING,
    //     state: STRING,
    //     locality: STRING,
    //     organization: STRING,
    //     organizationalUnit: STRING,
    //     commonName: STRING,
    //     email: STRING
    //     },
    //     duration
    // }
    event.preventDefault();
    if(!this.everythingOkay()) return;
    let response = await caService.add(this.subjectKeyIdentifier, {
        subjectName: {
            country: this.state.country,
            state: this.state.state,
            locality: this.state.locality,
            organization: this.state.organization,
            organizationalUnit: this.state.organizationalUnit,
            commonName: this.state.commonName,
            email: 'placeholder_until_registration_and_login_feature_arrives@gomalj.com'
        },
        duration: +this.state.duration_in_years
    });

    alert('Succesfully added CA.');

  }

  everythingOkay = () => {
    return this.state.regex_organization(this.state.organization) &&
      this.state.regex_organizationalUnit(this.state.organizationalUnit) &&
      this.state.regex_commonName(this.state.commonName) &&
      this.state.regex_country(this.state.country) &&
      this.state.regex_state(this.state.state) &&
      this.state.regex_locality(this.state.locality) &&
      this.state.regex_duration_in_years(this.state.duration_in_years)
  }
  
  render() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('pki_admin'))
    {
      return <Redirect to='/unauthorized' />
    }
    return (
      <div className="offset-lg-4 offset-md-2 col-lg-4 col-md-8 col-12">
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="organization">Organization</label>
            <input type="text" className="form-control" id="organization" aria-describedby="organizationHelp" placeholder="Enter organization" name="organization" onChange={this.handleChange}/> 
            <span class="valerror" style={{opacity: this.state.val_organization?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
            <label htmlFor="organizationalUnit">Organizational Unit</label>
            <input type="text" className="form-control" id="organizationalUnit" aria-describedby="organizationalUnitHelp" placeholder="Enter organizational unit" name="organizationalUnit" onChange={this.handleChange}/> 
            <span class="valerror" style={{opacity: this.state.val_organizationalUnit?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
            <label htmlFor="commonName">Common name</label>
            <input type="text" className="form-control" id="commonName" aria-describedby="commonNameHelp" placeholder="Enter common name" name="commonName" onChange={this.handleChange}/> 
            <span class="valerror" style={{opacity: this.state.val_commonName?0:1}}>Validation fail</span>
           </div>
          <div className="form-group">
            <label htmlFor="country">Country</label>
            <input type="text" className="form-control" id="country" aria-describedby="countryHelp" placeholder="Enter country" name="country" onChange={this.handleChange}/> 
            <span class="valerror" style={{opacity: this.state.val_country?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
            <label htmlFor="state">State</label>
            <input type="text" className="form-control" id="state" aria-describedby="stateHelp" placeholder="Enter state" name="state" onChange={this.handleChange}/>
            <span class="valerror" style={{opacity: this.state.val_state?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
            <label htmlFor="locality">Locality</label>
            <input type="text" className="form-control" id="locality" aria-describedby="localityHelp" placeholder="Enter locality" name="locality" onChange={this.handleChange}/> 
            <span class="valerror" style={{opacity: this.state.val_locality?0:1}}>Validation fail</span>
          </div>
          <div className="form-group">
            <label htmlFor="duration_in_years">Duration in years</label>
            <input type="number" className="form-control" id="duration_in_years" placeholder="Duration in years" name="duration_in_years" onChange={this.handleChange}/>
            <span class="valerror" style={{opacity: this.state.val_duration_in_years?0:1}}>Validation fail</span>
          </div>
          <button type="submit" disabled={!this.everythingOkay()} className="btn btn-primary col-8 offset-2 my-5">Submit</button>
        </form>
      </div>
    );
  }
}
export default AddCA;