import React, { Component } from "react";
import SortableTree from "react-sortable-tree";
import ReactSortableTreeThemeFileExplorer from "react-sortable-tree-theme-full-node-drag";
import agentService from "../Services/agentService";
import { Redirect } from 'react-router-dom'
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
} from "recharts";

class Agents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detail: {},
      agents: [],
      term: "",
      termA: "",
      termDF: "",
      termTF: "",
      termDT: "",
      termTT: "",
      from: null,
      to: null,
    };
  }

  async componentDidMount() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('siem_admin'))
    {
      return;
    }
    var raw_data = (await agentService.getAllForMe()).data.data;
    raw_data = raw_data.map((e) => {
      e.title = e.id;
      return e;
    });
    for (let i in raw_data) {
      raw_data[i].logs = JSON.parse(
        (await agentService.getLogsForAgent(raw_data[i].id)).data.data
      ).map(
        (e) =>
          `${e.level}|${e.timestamp}|${e.source}|${e.eventId}|${e.taskCategory}|${e.message}`
      );
      let alarmes = JSON.parse(
        (await agentService.getAlarmsForAgent(raw_data[i].id)).data.data
      );
      let timestamps = alarmes.map((e) => e.timestamp);
      let minTime = Math.min(...timestamps);
      let maxTime = Math.max(...timestamps);
      raw_data[i].alarmsVisual = [...Array(7)]
        .map((e, i) => minTime + ((i + 1) * (maxTime - minTime)) / 7)
        .map((e) => {
          return {
            time: Math.floor(e - minTime),
            alarms: alarmes.filter(
              (x) =>
                x.timestamp <= e && x.timestamp >= e - (maxTime - minTime) / 7
            ).length,
          };
        });
      raw_data[i].minTime = minTime;
      raw_data[i].alarms = alarmes;
    }
    var data_tree = [agentService.buildTree(raw_data)];
    this.setState({
      detail: undefined,
      agents: data_tree,
    });
  }

  changeDetailToMatchDateForm = async (detail, df, dt) => {
    if (df && dt) {
      detail.logs_date = JSON.parse(
        (await agentService.getLogsForAgentDate(detail.id, df, dt)).data.data
      ).map(
        (e) =>
          `${e.level}|${e.timestamp}|${e.source}|${e.eventId}|${e.taskCategory}|${e.message}`
      );
      detail.alarms_date = JSON.parse(
        (await agentService.getAlarmsForAgentDate(detail.id, df, dt)).data.data
      );
    }
    return detail;
  };

  processRegq = (x) => {
    return x.search(new RegExp(this.state.term, "i")) != -1;
  };
  processRegqAlarm = (x) => {
    return x.search(new RegExp(this.state.termA, "i")) != -1;
  };

  handleSubmit = (e) => {
    e.preventDefault();
  };
  handleChangeAlarm = (event) => {
    let val = event.target.value;
    this.setState({ termA: val });
  };

  handleChange = (event) => {
    let val = event.target.value;
    this.setState({ term: val });
  };

  handleChangeDateFrom = async (event) => {
    let val = event.target.value;
    if (val == "") return;
    if (this.state.termTF && this.state.termDT && this.state.termTT) {
      let df = new Date(`${val} ${this.state.termTF}`);
      let dt = new Date(`${this.state.termDT} ${this.state.termTT}`);
      this.setState({
        termDF: val,
        from: df,
        to: dt,
        detail: await this.changeDetailToMatchDateForm(
          this.state.detail,
          df,
          dt
        ),
      });
    } else {
      this.setState({ termDF: val });
    }
  };
  handleChangeTimeFrom = async (event) => {
    let val = event.target.value;
    if (val == "") return;
    if (this.state.termDF && this.state.termDT && this.state.termTT) {
      let df = new Date(`${this.state.termDF} ${val}`);
      let dt = new Date(`${this.state.termDT} ${this.state.termTT}`);
      this.setState({
        termTF: val,
        from: df,
        to: dt,
        detail: await this.changeDetailToMatchDateForm(
          this.state.detail,
          df,
          dt
        ),
      });
    } else {
      this.setState({ termTF: val });
    }
  };
  handleChangeDateTo = async (event) => {
    let val = event.target.value;
    if (val == "") return;
    if (this.state.termTF && this.state.termDF && this.state.termTT) {
      let df = new Date(`${this.state.termDF} ${this.state.termTF}`);
      let dt = new Date(`${val} ${this.state.termTT}`);
      this.setState({
        termDT: val,
        from: df,
        to: dt,
        detail: await this.changeDetailToMatchDateForm(
          this.state.detail,
          df,
          dt
        ),
      });
    } else {
      this.setState({ termDT: val });
    }
  };
  handleChangeTimeTo = async (event) => {
    let val = event.target.value;
    if (val == "") return;
    if (this.state.termTF && this.state.termDT && this.state.termDF) {
      let df = new Date(`${this.state.termDF} ${this.state.termTF}`);
      let dt = new Date(`${this.state.termDT} ${val}`);
      this.setState({
        termTT: val,
        from: df,
        to: dt,
        detail: await this.changeDetailToMatchDateForm(
          this.state.detail,
          df,
          dt
        ),
      });
    } else {
      this.setState({ termTT: val });
    }
  };

  dateTimeFormat = (value) => {
    return new Date(value).toLocaleDateString("en-gb", {
      month: "short",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit",
    });
  };
  formatTick = ({ x, y, payload }) => {
    return (
      <text x={x - 25} y={y + 8} fontSize="10">
        {this.dateTimeFormat(this.state.detail.minTime + payload.value)}
      </text>
    );
  };
  formatTickY = ({ x, y, payload }) => {
    return (
      <text x={x - 12} y={y + 3} fontSize="10" textAnchor="middle">
        {payload.value}
      </text>
    );
  };
  render() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('siem_admin'))
    {
      return <Redirect to='/unauthorized' />
    }
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-4 col-12 card" style={{ height: "84vh" }}>
            <SortableTree
              canDrag={false}
              treeData={this.state.agents}
              onChange={(agents) => this.setState({ agents })}
              theme={ReactSortableTreeThemeFileExplorer}
              generateNodeProps={({ node, path }) => ({
                buttons: [
                  <button
                    style={{
                      display: node.disabledDetails ? "none" : "inline-block",
                    }}
                    className="btn btn-primary"
                    onClick={async () => {
                      let detailedNode = node;
                      this.setState({
                        detail: await this.changeDetailToMatchDateForm(
                          detailedNode,
                          this.state.from,
                          this.state.to
                        ),
                      });
                    }}
                  >
                    Details
                  </button>,
                ],
              })}
            />
          </div>

          {this.state.detail && this.state.detail.id && (
            <div className="col-md-8 col-12">
              <table className="col-12 pr-2" style={{ height: "100%" }}>
                <tr style={{ height: "50%" }}>
                  <td
                    style={{ width: "50%" }}
                    className="p-2 border border-primary"
                  >
                    <LineChart
                      width={600}
                      height={300}
                      data={this.state.detail.alarmsVisual}
                      margin={{ top: 5, right: 40, bottom: 5, left: 0 }}
                    >
                      <Line type="monotone" dataKey="alarms" stroke="#8884d8" />
                      {/* <CartesianGrid stroke="#ccc" strokeDasharray="13 13" /> */}
                      <XAxis dataKey="time" tick={this.formatTick} />
                      <YAxis tick={this.formatTickY} />
                      <Tooltip />
                    </LineChart>
                  </td>
                  <td
                    style={{ width: "50%" }}
                    className="p-2 border border-primary"
                  >
                    <div
                      style={{
                        width: "100%",
                        minHeight: "11vh",
                        maxHeight: "11vh",
                      }}
                    >
                      <form>
                        <div className="form-group">
                          <label htmlFor="search">Search</label>
                          <input
                            type="text"
                            className="form-control"
                            id="alarmSearch"
                            aria-describedby="alarmSearchHelp"
                            placeholder="Enter regular expression"
                            name="search"
                            onChange={this.handleChangeAlarm}
                          />
                        </div>
                      </form>
                    </div>
                    <div
                      className="logs"
                      style={{
                        width: "100%",
                        minHeight: "31vh",
                        maxHeight: "31vh",
                        overflowY: "scroll",
                        fontFamily: "Consolas",
                      }}
                    >
                      {this.state.detail.alarms
                        .map(
                          (e) =>
                            `${e.agentId}|${e.category}|${e.timestamp}|${e.message}`
                        )
                        .filter(this.processRegqAlarm)
                        .map((e) => (
                          <p>{e}</p>
                        ))}
                    </div>
                  </td>
                </tr>
                <tr style={{ height: "50%" }}>
                  <td
                    style={{ width: "50%" }}
                    className="p-2 border border-primary"
                  >
                    <div
                      style={{
                        width: "100%",
                        minHeight: "11vh",
                        maxHeight: "11vh",
                      }}
                    >
                      <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                          <label htmlFor="search">Search</label>
                          <input
                            type="text"
                            className="form-control"
                            id="search"
                            aria-describedby="searchHelp"
                            placeholder="Enter regular expression"
                            name="search"
                            onChange={this.handleChange}
                          />
                        </div>
                      </form>
                    </div>
                    <div
                      className="logs"
                      style={{
                        width: "100%",
                        minHeight: "31vh",
                        maxHeight: "31vh",
                        overflowY: "scroll",
                        fontFamily: "Consolas",
                      }}
                    >
                      {this.state.detail.logs
                        .filter(this.processRegq)
                        .map((e) => (
                          <p>
                            <span className={e.split("|")[0]}>
                              {e.split("|")[0]}
                            </span>
                            |{e.split("|").slice(1).join("|")}
                          </p>
                        ))}
                    </div>
                  </td>
                  <td
                    style={{ width: "50%" }}
                    className="p-2 border border-primary"
                  >
                    <div
                      style={{
                        width: "100%",
                        minHeight: "11vh",
                        maxHeight: "11vh",
                      }}
                    >
                      <table style={{ width: "100%" }}>
                        <tr>
                          <td>
                            <form onSubmit={this.handleSubmit}>
                              <div className="form-group">
                                <label htmlFor="searchDateFrom">From:</label>
                                <input
                                  type="date"
                                  className="form-control"
                                  id="searchDateFrom"
                                  aria-describedby="searchDateFromHelp"
                                  placeholder="Date from"
                                  name="searchDateFrom"
                                  onChange={this.handleChangeDateFrom}
                                />
                                <input
                                  type="time"
                                  className="form-control"
                                  id="searchTimeFrom"
                                  aria-describedby="searchTimeFromHelp"
                                  placeholder="Time from"
                                  name="searchTimeFrom"
                                  onChange={this.handleChangeTimeFrom}
                                />
                              </div>
                            </form>
                          </td>
                          <td>
                            <form onSubmit={this.handleSubmit}>
                              <div className="form-group">
                                <label htmlFor="searchDateTo">To:</label>
                                <input
                                  type="date"
                                  className="form-control"
                                  id="searchDateTo"
                                  aria-describedby="searchDateToHelp"
                                  placeholder="Date to"
                                  name="searchDateTo"
                                  onChange={this.handleChangeDateTo}
                                />
                                <input
                                  type="time"
                                  className="form-control"
                                  id="searchTimeTo"
                                  aria-describedby="searchTimeToHelp"
                                  placeholder="Time to"
                                  name="searchTimeTo"
                                  onChange={this.handleChangeTimeTo}
                                />
                              </div>
                            </form>
                          </td>
                        </tr>
                      </table>
                    </div>
                    {this.state.detail &&
                      this.state.detail.logs_date &&
                      this.state.detail.alarms_date && (
                        <div>
                          <div
                            style={{
                              width: "100%",
                              minHeight: "2vh",
                              maxHeight: "2vh",
                            }}
                          >
                            <p>
                              Total logs: {this.state.detail.logs_date.length}{" "}
                              from [{this.dateTimeFormat(this.state.from)}] to [
                              {this.dateTimeFormat(this.state.to)}]
                            </p>
                          </div>
                          <div
                            className="logs"
                            style={{
                              width: "100%",
                              minHeight: "12.5vh",
                              maxHeight: "12.5vh",
                              overflowY: "scroll",
                              fontFamily: "Consolas",
                            }}
                          >
                            {this.state.detail.logs_date
                              .filter(this.processRegq)
                              .map((e) => (
                                <p>
                                  <span className={e.split("|")[0]}>
                                    {e.split("|")[0]}
                                  </span>
                                  |{e.split("|").slice(1).join("|")}
                                </p>
                              ))}
                          </div>
                          <hr></hr>
                          <div
                            style={{
                              width: "100%",
                              minHeight: "2vh",
                              maxHeight: "2vh",
                            }}
                          >
                            <p>
                              Total alarms:{" "}
                              {this.state.detail.alarms_date.length} from [
                              {this.dateTimeFormat(this.state.from)}] to [
                              {this.dateTimeFormat(this.state.to)}]
                            </p>
                          </div>
                          <div
                            className="logs"
                            style={{
                              width: "100%",
                              minHeight: "12.5vh",
                              maxHeight: "12.5vh",
                              overflowY: "scroll",
                              fontFamily: "Consolas",
                            }}
                          >
                            {this.state.detail.alarms_date
                              .map(
                                (e) =>
                                  `${e.agentId}|${e.category}|${e.timestamp}|${e.message}`
                              )
                              .filter(this.processRegqAlarm)
                              .map((e) => (
                                <p>{e}</p>
                              ))}
                          </div>
                        </div>
                      )}

                    {this.state.detail &&
                      (!this.state.detail.logs_date ||
                        !this.state.detail.alarms_date) && (
                        <div
                          style={{
                            width: "100%",
                            minHeight: "29vh",
                            maxHeight: "29vh",
                          }}
                        ></div>
                      )}
                  </td>
                </tr>
              </table>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default Agents;
