import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import userService from '../Services/userService';

class Unauthorized extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  async componentDidMount() {
  }

  render() {
    return (
      <h1>Unauthorized!</h1>
    );
  }
  
}

export default Unauthorized;