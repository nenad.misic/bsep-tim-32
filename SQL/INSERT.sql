CREATE TABLE bsep.pki.certificate_authority (
    "subject" VARCHAR(255) NOT NULL,
    "issuer" VARCHAR(255) NOT NULL,
    "subject_key_identifier" VARCHAR(255) NOT NULL,
    "authority_key_identifier" VARCHAR(255) NOT NULL,
    "serial_number" BIGINT NOT NULL,
    "pem" TEXT NOT NULL,
    "private_key" TEXT NOT NULL,
    "serial_counter" integer NOT NULL DEFAULT '1',
   	"not_before" bigint NOT NULL,
	  "not_after" bigint NOT NULL,
    "revoked" boolean NOT NULL DEFAULT 'false',
    CONSTRAINT "certificate_authority_pk" PRIMARY KEY ("subject_key_identifier")
) WITH (
  OIDS=FALSE
);



ALTER TABLE bsep.pki.certificate_authority ADD CONSTRAINT "certificate_authority_fk0" FOREIGN KEY ("authority_key_identifier") REFERENCES bsep.pki.certificate_authority("subject_key_identifier");


CREATE TABLE bsep.pki.certificate (
    "subject" VARCHAR(255) NOT NULL,
    "issuer" VARCHAR(255) NOT NULL,
    "subject_key_identifier" VARCHAR(255) NOT NULL,
    "authority_key_identifier" VARCHAR(255) NOT NULL,
    "serial_number" BIGINT NOT NULL,
    "pem" TEXT NOT NULL,
    "revoked" boolean NOT NULL DEFAULT 'false',
   	"not_before" bigint NOT NULL,
	  "not_after" bigint NOT NULL,
    CONSTRAINT "certificate_pk" PRIMARY KEY ("subject_key_identifier")
) WITH (
  OIDS=FALSE
);



ALTER TABLE bsep.pki.certificate ADD CONSTRAINT "certificate_fk0" FOREIGN KEY ("authority_key_identifier") REFERENCES bsep.pki.certificate_authority("subject_key_identifier");


CREATE TABLE bsep.pki.user (
    "username" VARCHAR(255) PRIMARY KEY,
    "password" VARCHAR(255) NOT NULL,
    "siem_center_id" INTEGER,
    "ca_skid" VARCHAR(255)
) WITH (
  OIDS=FALSE
);


ALTER TABLE bsep.pki.user ADD CONSTRAINT "user_certificate_authority_fk0" FOREIGN KEY ("ca_skid") REFERENCES bsep.pki.certificate_authority("subject_key_identifier");
ALTER TABLE bsep.pki.user ADD CONSTRAINT "user_siem_center_fk0" FOREIGN KEY ("siem_center_id") REFERENCES bsep.pki.siem_center("id");

CREATE TABLE bsep.pki.user_role (
    "name" VARCHAR(255) NOT NULL,
    CONSTRAINT "role_pk" PRIMARY KEY ("name")
) WITH (
  OIDS=FALSE
);

CREATE TABLE bsep.pki.user_user_role (
    "id" SERIAL PRIMARY KEY,
    "user_id" VARCHAR(255) NOT NULL,
    "role_id" VARCHAR(255) NOT NULL
) WITH (
  OIDS=FALSE
);

ALTER TABLE bsep.pki.user_user_role ADD CONSTRAINT "user_user_role_fk0" FOREIGN KEY ("user_id") REFERENCES bsep.pki.user("username");
ALTER TABLE bsep.pki.user_user_role ADD CONSTRAINT "user_user_role_fk1" FOREIGN KEY ("role_id") REFERENCES bsep.pki.user_role("name");



INSERT INTO bsep.pki.user_role (name) VALUES ('pki_admin');
INSERT INTO bsep.pki.user_role (name) VALUES ('siem_admin');


CREATE TABLE pki.siem_center
(
    "id" SERIAL PRIMARY KEY,
    name character varying(255) COLLATE pg_catalog."default",
    certificate character varying(255) COLLATE pg_catalog."default",
    certificate_authority character varying(255) COLLATE pg_catalog."default",
    password character varying(255) COLLATE pg_catalog."default",
    parent_siem integer,
    CONSTRAINT certificate_authority_fk FOREIGN KEY (certificate_authority)
        REFERENCES pki.certificate_authority (subject_key_identifier),
    CONSTRAINT certificate_fk FOREIGN KEY (certificate)
        REFERENCES pki.certificate (subject_key_identifier),
    CONSTRAINT parent_siem_fk FOREIGN KEY (parent_siem)
        REFERENCES pki.siem_center (id)
);

ALTER TABLE pki.siem_center
    OWNER to postgres;


CREATE TABLE pki.siem_agent
(
  "id" SERIAL PRIMARY KEY,
  "approved" BOOLEAN NOT NULL DEFAULT 'false',
  "certificate" VARCHAR(255),
  "siem_center" INTEGER,
  "password" VARCHAR(255),
  CONSTRAINT certificate_fk FOREIGN KEY (certificate) REFERENCES pki.certificate (subject_key_identifier),
  CONSTRAINT siem_center_fk FOREIGN KEY (siem_center) REFERENCES pki.siem_center (id)
);