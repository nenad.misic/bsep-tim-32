import utility

class CertificateBean:
    def __init__(self, certificate, certificate_pem, revoked=False):
        self.serial_number = certificate.get_serial_number()
        
        self.subject = utility.get_formatted_subject(certificate)
        self.issuer = utility.get_formatted_issuer(certificate)
        self.subject_key_identifier = utility.get_subject_key_identifier(certificate)
        self.authority_key_identifier = utility.get_authority_key_identifier(certificate)

        self.pem = certificate_pem
        self.not_before = utility.decode_asn_time(certificate.get_notBefore())
        self.not_after = utility.decode_asn_time(certificate.get_notAfter())
        self.revoked = revoked

    def __str__(self):
        text = 'Certificate\n'
        text += 'Serial number: ' + str(self.serial_number) + "\n"
        text += 'Subject: ' + self.subject + '\n'
        text += 'Issuer: ' + self.issuer + '\n'
        text += 'Not before: ' + str(self.not_before) + '\n'
        text += 'Not after: ' + str(self.not_after) + '\n'
        text += 'Subject key identifier: ' + self.authority_key_identifier + '\n'
        text += 'Authority key identifier: ' + self.authority_key_identifier + '\n'
        text += 'Revoked: ' + ('YES' if self.revoked else 'NO') + '\n'
        return text

    def get_dict(self):
        object_dict = {
            'serialNumber': self.serial_number,
            'subject': self.subject,
            'issuer': self.issuer,
            'subjectKeyIdentifier': self.subject_key_identifier,
            'authorityKeyIdentifier': self.authority_key_identifier,
            'pem': self.pem,
            'notBefore': self.not_before,
            'notAfter': self.not_after,
            'revoked': self.revoked,
            'ca': False
        }
        return object_dict
    
"""subject(string) | issuer(string) | subjectKeyIdentifier(string) | authorityKeyIdentifier(string) | serial_number(int) | pem(string) | private_key(string) | serial_counter(int, default 1)"""
