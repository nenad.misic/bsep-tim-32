import json

def siem_centre_tuple_to_dict(tuple):
    return {
        'id': tuple[0],
        'name': tuple[1],
        'certificate': tuple[2],
        'certificate_authority': tuple[3],
        'password': tuple[4],
        'parent_siem': tuple[5]
    }

def siem_agent_tuple_to_dict(tuple):
    return {
        'id': tuple[0],
        'approved': tuple[1],
        'certificate': tuple[2],
        'siem_center': tuple[3],
        'password': tuple[4],
    }

def dblog_to_dict(log):
    return {
        "agentId": log['agentId'],
        "level": log['level'],
        "timestamp": log['timestamp'],
        "source": log['source'],
        "eventId": log['eventId'],
        "taskCategory": log['taskCategory'],
        "message": log['message']
    }

def alarm_to_dict(alarm):
    return json.loads(str(alarm))