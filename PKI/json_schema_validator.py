from jsonschema import validate
from jsonschema.exceptions import ValidationError
from werkzeug.exceptions import BadRequest

def validate_ca(json_data):
    schema = {
        'type': 'object',
        "required": [ 'subjectName', 'duration' ],
        'properties': {
            'subjectName': {
                'type': 'object',
                'properties': {
                    'country': {
                        'type': 'string',
                        'minLength': 2,
                        'maxLength': 2
                    },
                    'state': {
                        'type': 'string'
                    },
                    'locality': {
                        'type': 'string'
                    },
                    'organization': {
                        'type': 'string'
                    },
                    'organizationalUnit': {
                        'type': 'string'
                    },
                    'commonName': {
                        'type': 'string'
                    },
                    'email': {
                        'type': 'string',
                        # RFC5222 email regex
                        'pattern': '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.)\{3\}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])'
                    }
                }
            },
            'duration': {
                'type': 'number',
                'minimum': 1,
                'maximum': 20
            }
        }
    }
    try:
        return validate(json_data, schema)
    except ValidationError as err:
        raise BadRequest(description=err.message)

def validate_certificate(json_data):
    schema = {
        'type': 'object',
        "required": [ 'subjectName', 'duration', 'publicKey' ],
        'properties': {
            'subjectName': {
                'type': 'object',
                'properties': {
                    'country': {
                        'type': 'string',
                        'minLength': 2,
                        'maxLength': 2
                    },
                    'state': {
                        'type': 'string'
                    },
                    'locality': {
                        'type': 'string'
                    },
                    'organization': {
                        'type': 'string'
                    },
                    'organizationalUnit': {
                        'type': 'string'
                    },
                    'commonName': {
                        'type': 'string'
                    },
                    'email': {
                        'type': 'string',
                        # RFC5222 email regex
                        'pattern': '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.)\{3\}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])'
                    }
                }
            },
            'duration': {
                'type': 'number',
                'minimum': 0,
                'maximum': 20
            },
            'publicKey': {
                'type': 'string',
                'minLength': 10
            }
        }
    }
    try:
        return validate(json_data, schema)
    except ValidationError as err:
        raise BadRequest(description=err.message)

def validate_siem_certificate(json_data):
    schema = {
        'type': 'object',
        "required": [ 'name', 'passphrase' ],
        'properties': {
            'name': {
                'type': 'string'
            },
            'passphrase': {
                'type': 'string'
            }
        }
    }
    try:
        return validate(json_data, schema)
    except ValidationError as err:
        raise BadRequest(description=err.message)

def validate_agent_request(json_data):
    schema = {
        'type': 'object',
        "required": [ 'siem_center_name' ],
        'properties': {
            'siem_center_name': {
                'type': 'string'
            }
        }
    }
    try:
        return validate(json_data, schema)
    except ValidationError as err:
        raise BadRequest(description=err.message)

def validate_get_agent_certificate(json_data):
    schema = {
        'type': 'object',
        "required": [ 'id', 'password', 'subjectName', 'publicKey' ],
        'properties': {
            'id': {
                'type': 'number'
            },
            'password': {
                'type': 'string'
            },
            'subjectName': {
                'type': 'object',
                'properties': {
                    'country': {
                        'type': 'string',
                        'minLength': 2,
                        'maxLength': 2
                    },
                    'state': {
                        'type': 'string'
                    },
                    'locality': {
                        'type': 'string'
                    },
                    'organization': {
                        'type': 'string'
                    },
                    'organizationalUnit': {
                        'type': 'string'
                    },
                    'commonName': {
                        'type': 'string'
                    },
                    'email': {
                        'type': 'string',
                        # RFC5222 email regex
                        'pattern': '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.)\{3\}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])'
                    }
                }
            },
            'publicKey': {
                'type': 'string'
            }
        }
    }
    try:
        return validate(json_data, schema)
    except ValidationError as err:
        raise BadRequest(description=err.message)

