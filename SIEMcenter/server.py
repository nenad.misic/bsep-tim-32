
from flask import Flask, render_template, request, jsonify
import werkzeug.serving
import ssl
import OpenSSL
import requests
from os import path
import db_service
import os
import rule_based_system
from datetime import datetime
from pyDatalog import pyDatalog
import utility
from werkzeug.exceptions import HTTPException, BadRequest
import json


pyDatalog.create_terms('X,Y,Z,W,count_username,count_agent,count_register,count_register,count_agent,count_generic,Z1,Z2,Z3,Z4,Z5')

ssl.match_hostname = lambda cert, hostname: hostname == cert['subjectAltName'][0][1]

CERTIFICATE_DIRECTORY = './certificates/'
PKI_URL = 'https://localhost:5000/'
SIEM_PASSPHRASE = 'siem'

class PeerCertWSGIRequestHandler( werkzeug.serving.WSGIRequestHandler ):
    """
    We subclass this class so that we can gain access to the connection
    property. self.connection is the underlying client socket. When a TLS
    connection is established, the underlying socket is an instance of
    SSLSocket, which in turn exposes the getpeercert() method.

    The output from that method is what we want to make available elsewhere
    in the application.
    """
    def make_environ(self):
        """
        The superclass method develops the environ hash that eventually
        forms part of the Flask request object.

        We allow the superclass method to run first, then we insert the
        peer certificate into the hash. That exposes it to us later in
        the request variable that Flask provides
        """
        environ = super(PeerCertWSGIRequestHandler, self).make_environ()
        x509_binary = self.connection.getpeercert(True)
        x509 = OpenSSL.crypto.load_certificate( OpenSSL.crypto.FILETYPE_ASN1, x509_binary )
        environ['peercert'] = x509
        return environ

        



app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'hello'

@app.route('/log', methods=['POST'])
def post_log():
    # check if client certificate is revoked
    agent_cert = request.environ['peercert']
    skid = agent_cert.get_extension(1)

    response = requests.get(PKI_URL+'certificate/' + str(skid) + '/check_revoked', verify=CERTIFICATE_DIRECTORY + 'rootCA.crt')
    revoked = response.json()['revoked']
    if revoked:
        raise BadRequest(description="Client certificate revoked")
        

    logs = request.json['logs']
    db_service.save_logs(logs)
    for log in logs:
        check_log_alarms(log)

    return jsonify({"message": "success"})


def check_log_alarms(log):
    timestamp = int(datetime.now().timestamp() * 1000) - 86400000 #1 day
    relevant_logs = db_service.get_relevant_logs(log['agentId'], log['message'], timestamp)
    alarms = rule_based_system.fire_all_rules(utility.dblog_to_dict(log), relevant_logs)
    if len(alarms) != 0:
        db_service.save_alarms(alarms)

@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response



if __name__ == "__main__":
    if not path.exists(CERTIFICATE_DIRECTORY):
        os.mkdir(CERTIFICATE_DIRECTORY)

    if not path.exists(CERTIFICATE_DIRECTORY + 'siem_cert.crt'):
        key = OpenSSL.crypto.PKey()
        key.generate_key(OpenSSL.crypto.TYPE_RSA, 2048)
        private_key = OpenSSL.crypto.dump_privatekey(OpenSSL.crypto.FILETYPE_PEM, key).decode('ascii')
        public_key = OpenSSL.crypto.dump_publickey(OpenSSL.crypto.FILETYPE_PEM, key).decode('ascii')
        result = requests.post(PKI_URL+'siem/certificate', json={'publicKey': public_key, 'passphrase': SIEM_PASSPHRASE, 'name': 'O=DefenceFirst, OU=SIEM_CENTER, CN=defencefirst, C=RS, ST=Vojvodina'}, verify=CERTIFICATE_DIRECTORY + 'rootCA.crt')
        result = result.json()
        certificate = result['cert']
        bundle = result['bundle']

        f_key = open(CERTIFICATE_DIRECTORY + 'siem_cert.key', 'w')
        f_key.write(private_key)
        f_key.close()

        f_cert = open(CERTIFICATE_DIRECTORY + 'siem_cert.crt', 'w')
        f_cert.write(certificate)
        f_cert.close()

        f_bundle = open(CERTIFICATE_DIRECTORY + 'bundle.crt', 'w')
        f_bundle.write(bundle)
        f_bundle.close()

    # app_key_password here is None, because the key isn't password protected,
    # but if yours is protected here's where you place it.
    app_key = CERTIFICATE_DIRECTORY + 'siem_cert.key'
    app_key_password = None
    app_cert = CERTIFICATE_DIRECTORY + 'siem_cert.crt'

    # in order to verify client certificates we need the certificate of the
    # CA that issued the client's certificate. In this example I have a
    # single certificate, but this could also be a bundle file.
    ca_cert = CERTIFICATE_DIRECTORY + 'bundle.crt'

    # create_default_context establishes a new SSLContext object that
    # aligns with the purpose we provide as an argument. Here we provide
    # Purpose.CLIENT_AUTH, so the SSLContext is set up to handle validation
    # of client certificates.
    ssl_context = ssl.create_default_context( purpose=ssl.Purpose.CLIENT_AUTH,
                                            cafile=ca_cert )

    # load in the certificate and private key for our server to provide to clients.
    # force the client to provide a certificate.
    ssl_context.load_cert_chain( certfile=app_cert, keyfile=app_key, password=app_key_password )
    ssl_context.verify_mode = ssl.CERT_REQUIRED
    
    # to establish an SSL socket we need the private key and certificate that
    # we want to serve to users.
    app.run( ssl_context=ssl_context, request_handler=PeerCertWSGIRequestHandler, port=5002 )