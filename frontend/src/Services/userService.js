import {api_sso} from '../Environment/environment';
import axios from 'axios';
import jwt from 'jwt-decode';

let userService = {
    register: async function (username, password, role) {
        let url = `${api_sso}register`;
        let res = await axios.post(url, {username: username, password: password, roles: [role]});
        if (res.msg == 'success') {
            return true;
        }
    },
    login: async function (username, password) {
        let url = `${api_sso}auth`;
        let access_token = await axios.post(url, {username: username, password: password});
        if (access_token.data && access_token.data.access_token)  {
            localStorage.setItem('user-token', access_token.data.access_token);
            localStorage.setItem('identity', JSON.stringify(jwt(access_token.data.access_token).identity));
            localStorage.setItem('expires', jwt(access_token.data.access_token).exp*1000);
            this.onChange(1);
            console.log(this.onChange)
            return true;
        
        }
        return false;
    },
    logout: function () {
        localStorage.removeItem('user-token');
        localStorage.removeItem('identity');
        localStorage.removeItem('expires');
        this.onChange(0);
    },
    onChange: function (v) {}
};

export default userService;

