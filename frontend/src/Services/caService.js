import {api_pki, bearer} from '../Environment/environment';
import axios from 'axios';
import { resolve } from 'dns';

let caService = {
    getAll: async () => {
        let url = `${api_pki}ca`;
        return axios.get(url, bearer());
    },
    getById: (skid) => {
        let url = `${api_pki}certificate/${skid}`;
        return axios.get(url, bearer());
    },
    add: (skid, ca) => {
        // {
        //     subjectName: {
        //     country: STRING,
        //     state: STRING,
        //     locality: STRING,
        //     organization: STRING,
        //     organizationalUnit: STRING,
        //     commonName: STRING,
        //     email: STRING
        //     },
        //     duration
        // }
        let url = `${api_pki}ca/${skid}`;
        return axios.post(url, ca, bearer());
    },
    revoke: (skid) => {
        let url = `${api_pki}certificate/${skid}/revoke`;
        return axios.get(url, bearer());
    },
    buildTree: (data) => {
        data.map(e => {
            e.children = data.filter(x => x.authorityKeyIdentifier === e.subjectKeyIdentifier && x.authorityKeyIdentifier !== x.subjectKeyIdentifier);
        });
        let root = data.filter(e => e.subjectKeyIdentifier === e.authorityKeyIdentifier)[0];
        console.log(root);
        return root;
    }
};

export default caService

