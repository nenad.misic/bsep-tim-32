import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import NavBar from './NavBar/NavBar';
import Login from './Login/Login';
import Register from './Register/Register';
import Certificates from './Certificates/Certificates';
import SIEMs from './SIEMs/SIEMs';
import Agents from './Agents/Agents';
import SIEM from './SIEMs/Detail/SIEM';
import AddCertificate from './Certificates/Add/AddCertificate';
import AddCA from './Certificates/Add/AddCA';
import AddSiem from './SIEMs/Add/AddSiem';
import AddRule from './Rules/AddRule';
import CADetail from './Certificates/Detail/CADetail';
import Welcome from './Welcome/Welcome.js';
import Unauthorized from './Unauthorized/Unauthorized.js';
import CertificateDetail from './Certificates/Detail/CertificateDetail';
import UnapprovedAgents from './UnapprovedAgents/UnapprovedAgents';

class App extends Component {

  render() {
    return (
      <div className="App">
        <NavBar></NavBar>
        <Route exact path='/' component={Welcome}/>
        <Route exact path='/unauthorized' component={Unauthorized}/>
        <Route exact path='/pki' component={Certificates}/>
        <Route exact path='/siems' component={SIEMs}/>
        <Route exact path='/agents' component={Agents}/>
        <Route exact path='/siem/:id' component={SIEM}/>
        <Route exact path='/add-cert/:subjectKeyIdentifier' component={AddCertificate}/>
        <Route exact path='/add-ca/:subjectKeyIdentifier' component={AddCA}/>
        <Route exact path='/add-siem/:id' component={AddSiem}/>
        <Route exact path='/ca/:subjectKeyIdentifier' component={CADetail}/>
        <Route exact path='/cert/:subjectKeyIdentifier' component={CertificateDetail}/>
        <Route exact path='/login' component={Login}/>
        <Route exact path='/rule' component={AddRule}/>
        <Route exact path='/unapproved' component={UnapprovedAgents}/>
        {/* <Route exact path='/register' component={Register}/> */}
        {/* Primeri ruta */}
        {/* <Route exact path='/' component={NekaKomponenta}/>
        <Route exact path='/stagod' component={NekaKomponenta}/>
        <Route exact path='/stagod2/:param_id_recimo' component={NekaKomponenta}/> */}
      </div>
    );
  }
}

export default App;