import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import siemService from '../Services/siemService'
import agentService from '../Services/agentService';
import { Redirect } from 'react-router-dom'

class UnapprovedAgents extends Component {
  constructor(props) {
    super(props);
    this.id = '';
    this.state = {
        agents: [],
    };

    
    this.handleSubmit = this.handleSubmit.bind(this);
    this.input = React.createRef();
  }

  async componentDidMount() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('siem_admin'))
    {
      return;
    }
    this.id = JSON.parse(localStorage.getItem('identity'))['siem_center_id']
    let agents = await siemService.getUnapprovedAgents(this.id)
    this.setState({agents: agents.data.data})
  }

  handleChange = (event) => {
    let ag = JSON.parse(JSON.stringify(this.state.agents));
    ag[+event.target.id].passphrase = event.target.value;
    this.setState({agents: ag});
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    let agent = this.state.agents[+event.target.id]
    if (agent.passphrase == '') return
    let res = await agentService.approveAgent(agent.id, agent.passphrase)
    alert('Agent approved')

  }

  render() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('siem_admin'))
    {
      return <Redirect to='/unauthorized' />
    }
    return (
        <div className="container">
            <div className="row">
                <div className="col-lg-8 col-md-12 offset-lg-2">
                    {this.state.agents && this.state.agents.length > 0 && (
                        <ul className="list-group">
                            {this.state.agents.map((a,i) => (
                                <li className="list-group-item m-2 border-top">
                                    <table style={{"width": "100%"}}>
                                        <tr>
                                            <td width="20%">
                                    Agent id: {a.id}</td>
                                            <td width="60%">
                                                <input id={i} type="password" placeholder="Passphrase" className="form-control" onChange={this.handleChange}></input>
                                            </td>
                                            <td align="right" width="20%"><button className="btn btn-info" id={i} onClick={this.handleSubmit}>Approve</button></td>
                                        </tr>
                                    </table>
                                </li>
                            ))}
                        </ul>
                    )}
                </div>
            </div>
        </div>
    );
  }
  
}

export default UnapprovedAgents;