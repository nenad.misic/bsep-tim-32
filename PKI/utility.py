from pyasn1.codec.der.decoder import decode as asn1_decoder
from pyasn1_modules.rfc5280 import SubjectKeyIdentifier, AuthorityKeyIdentifier
from pyasn1.type.useful import UTCTime

def years_to_millis(years):
    return years * 31536000

def milis_to_years(milis):
    return milis / 31536000000

def hex_str_to_colon(hex_str):
    hex_str = hex_str[2:].upper()
    return ':'.join(hex_str[i: i+2] for i in range(0, len(hex_str), 2))

def get_subject_key_identifier(certificate):
    decoded_subject_key_id, _ = asn1_decoder(certificate.get_extension(1).get_data(), asn1Spec=SubjectKeyIdentifier())
    return hex_str_to_colon(decoded_subject_key_id.prettyPrint())

def get_authority_key_identifier(certificate):
    auth_key = certificate.get_extension(2).get_data()
    decoded_auth_key_id, _ = asn1_decoder(auth_key, asn1Spec=AuthorityKeyIdentifier())
    return hex_str_to_colon(decoded_auth_key_id.getComponentByName('keyIdentifier').prettyPrint())

def get_formatted_subject(certificate):
    return ', '.join('{}={}'.format(*map(lambda x: x.decode('utf-8'), t)) for t in certificate.get_subject().get_components())

def get_formatted_issuer(certificate):
    return ', '.join('{}={}'.format(*map(lambda x: x.decode('utf-8'), t)) for t in certificate.get_issuer().get_components())

def decode_asn_time(asn_time):
    return UTCTime(asn_time[2:]).asDateTime.timestamp() * 1000

def ca_tuple_to_dict(ca_tuple):
    return {
        'serialNumber': ca_tuple[4],
        'subject': ca_tuple[0],
        'issuer': ca_tuple[1],
        'subjectKeyIdentifier': ca_tuple[2],
        'authorityKeyIdentifier': ca_tuple[3],
        'pem': ca_tuple[5],
        'not_before': ca_tuple[8],
        'not_after': ca_tuple[9],
        'revoked': ca_tuple[10],
        'ca': True
    }

def ca_bean_to_dict(ca_bean):
    return {
        'serialNumber': ca_bean.serial_number,
        'subject': ca_bean.subject,
        'issuer': ca_bean.issuer,
        'subjectKeyIdentifier': ca_bean.subject_key_identifier,
        'authorityKeyIdentifier': ca_bean.authority_key_identifier,
        'pem': ca_bean.pem,
        'not_before': ca_bean.not_before,
        'not_after': ca_bean.not_after,
        'revoked': ca_bean.revoked,
        'ca': True
    }

def certificate_tuple_to_dict(cert_tuple):
    print(cert_tuple)
    return {
        'serialNumber': cert_tuple[4],
        'subject': cert_tuple[0],
        'issuer': cert_tuple[1],
        'subjectKeyIdentifier': cert_tuple[2],
        'authorityKeyIdentifier': cert_tuple[3],
        'pem': cert_tuple[5],
        'not_before': cert_tuple[7],
        'not_after': cert_tuple[8],
        'revoked': cert_tuple[6],
        'ca': False
    }




if __name__ == '__main__':
    pass


def subject_name_to_dict(subject_name):
    # O=Defence First, OU=SIEMcenter, CN=SIEMcenterCA, ST=Vojvodina, C=RS
    names = tuple(map(lambda x: x.split('=')[1], subject_name.split(',')))
    return {
        'organization': names[0],
        'organizationalUnit': names[1],
        'commonName': names[2],
        'country': names[3],
        'state': names[4]
    }