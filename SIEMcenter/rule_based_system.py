from pyDatalog import pyDatalog, Logic
import datetime
import json
import utility

LOGIN_USERNAME_TIME = 2 * 60000 # two minutes
LOGIN_USERNAME_COUNT = 3
LOGIN_AGENT_TIME = 2 * 600000 # two minutes
LOGIN_AGENT_COUNT = 3
REGISTER_TIME = 2 * 60000 # two minutes
REGISTER_COUNT = 3
AGENT_TIME = 1 * 60000
AGENT_COUNT = 100

ALARMS_PATH = 'alarms.json'
# ALARMS_PATH = './SIEMcenter/alarms.json' # DEBUG

pyDatalog.create_terms('X,Y,Z,W,count_username,count_agent,count_register,count_register,count_agent,count_generic,Z1,Z2,Z3,Z4,Z5')


class LogMixin(pyDatalog.Mixin):
    def __init__(self, agentId, level, timestamp, source, eventId, taskCategory, message): 
        # call the initialization method of the Mixin class
        super(LogMixin, self).__init__()
        self.agentId = agentId
        self.level = level
        self.timestamp = timestamp
        self.source = source
        self.eventId = eventId
        self.taskCategory = taskCategory
        self.message = message
    
    def __repr__(self): # specifies how to display an Employee
        return str(self.agentId) + ' | ' + self.level + ' | ' + str(self.timestamp) + ' | ' + self.source + ' | ' + str(self.eventId) + ' | ' + self.taskCategory + ' | ' + self.message

class AlarmMixin(pyDatalog.Mixin):
    def __init__(self, agentId, category, timestamp, message, code): 
        # call the initialization method of the Mixin class
        super(AlarmMixin, self).__init__()
        self.agentId = agentId
        self.category = category
        self.timestamp = timestamp
        self.message = message
        self.code = code
        self.active = False
    
    def __repr__(self): # specifies how to display an Employee
        return json.dumps({'agentId': self.agentId, 'category': self.category, 'timestamp': self.timestamp, 'message': self.message})

def create_alarms(agentId, username, eventId):
    return [
        AlarmMixin(agentId, 'Login', int(datetime.datetime.now().timestamp() * 1000), 'Too many login attempts with username: ' + username, 0),
        AlarmMixin(agentId, 'Login', int(datetime.datetime.now().timestamp() * 1000), 'Too many login attempts from agent: ' + str(agentId), 1),
        AlarmMixin(agentId, 'Any', int(datetime.datetime.now().timestamp() * 1000), 'Error found; event id: ' + str(eventId), 2),
        AlarmMixin(agentId, 'Register', int(datetime.datetime.now().timestamp() * 1000), 'Too many registrations from agent: ' + str(agentId), 3),
        AlarmMixin(agentId, 'Any', int(datetime.datetime.now().timestamp() * 1000), 'Too many logs from agent: ' + str(agentId), 4)
    ]

def load_alarm_descriptions():
    alarm_descriptions = []
    with open(ALARMS_PATH, 'r') as f:
        alarm_descriptions = json.loads(f.read())
    return alarm_descriptions

def create_added_alarms(alarm_descriptions, agentId):
    alarms = []
    for ad in alarm_descriptions:
        alarms.append(AlarmMixin(agentId, ad['taskCategory'], int(datetime.datetime.now().timestamp() * 1000), ad['info_message'], 1337))

    return alarms
    

def fire_all_rules(most_recent_log, relevant_logs):
    Logic()

    mixins = list(map(lambda x: LogMixin(**utility.dblog_to_dict(x)), relevant_logs))


    alarms = create_alarms(most_recent_log['agentId'], most_recent_log['message'], most_recent_log['eventId'])

    alarm_descriptions = load_alarm_descriptions()
    added_alarms = create_added_alarms(alarm_descriptions, most_recent_log['agentId'])

    login_username_rule(most_recent_log['message'], alarms)
    login_agent_rule(most_recent_log['agentId'], alarms)
    error_rule(most_recent_log, alarms)
    register_rule(most_recent_log['agentId'], alarms)
    request_agent_rule(most_recent_log['agentId'], alarms)

    for i in range(len(alarm_descriptions)):
        print(generic_rule(most_recent_log, added_alarms, alarm_descriptions[i], i))

    result = (AlarmMixin.active[X]==True).data
    retval = list(map(lambda x: utility.alarm_to_dict(x[0]), result))
    return retval

def login_username_rule(username, alarms):
    current_time = datetime.datetime.now().timestamp() * 1000

    (count_username[X]==len_(Y)) <= ((LogMixin.message[Y]==X) & (LogMixin.timestamp[Y] > current_time - LOGIN_USERNAME_TIME) & (LogMixin.taskCategory[Y]=='Login'))
    evaluation = (count_username[username]==Z)
    result = evaluation[0][0]>= LOGIN_USERNAME_COUNT if len(evaluation) > 0 else False

    alarms[0].active = result

    return result

def login_agent_rule(agentId, alarms):
    current_time = datetime.datetime.now().timestamp() * 1000

    (count_agent[X]==len_(Y)) <= ((LogMixin.agentId[Y]==X) & (LogMixin.timestamp[Y] > current_time - LOGIN_AGENT_TIME) & (LogMixin.taskCategory[Y]=='Login') & (LogMixin.level[Y]!='INFO'))
    evaluation = (count_agent[agentId]==Z)
    result = evaluation[0][0] >= LOGIN_AGENT_COUNT if len(evaluation) > 0 else False

    alarms[1].active = result

    return result

def error_rule(log, alarms):
    result = log['level'] == 'ERROR'
    alarms[2].active = result
    return result


def register_rule(agentId, alarms):
    current_time = datetime.datetime.now().timestamp() * 1000

    (count_register[X]==len_(Y)) <= ((LogMixin.agentId[Y]==X) \
        & (LogMixin.timestamp[Y] > current_time - REGISTER_TIME) \
        & (LogMixin.taskCategory[Y]=='Register') \
        & (LogMixin.level[Y]=='INFO'))
    evaluation = (count_register[agentId]==Z)
    result = evaluation[0][0] >= REGISTER_COUNT if len(evaluation) > 0 else False

    alarms[3].active = result

    return result
    

def request_agent_rule(agentId, alarms):
    current_time = datetime.datetime.now().timestamp() * 1000

    (count_agent[X]==len_(Y)) <= ((LogMixin.agentId[Y]==X) & (LogMixin.timestamp[Y] > current_time - AGENT_TIME))
    evaluation = (count_agent[agentId]==Z)
    result = evaluation[0][0] >= AGENT_COUNT if len(evaluation) > 0 else False

    alarms[4].active = result

    return result

def generic_rule(log, alarms, alarm_description, alarm_index):
    current_time = datetime.datetime.now().timestamp() * 1000

    (count_generic[X]==len_(Y)) <= \
        ((LogMixin.agentId[Y]==(log['agentId'] if alarm_description['agentId'] else Z)) \
        & (LogMixin.level[Y]==(alarm_description['level'] if alarm_description['level'] != '' else Z1)) \
        & (LogMixin.source[Y]==(log['source'] if alarm_description['source'] else Z2)) \
        & (LogMixin.eventId[Y]==(alarm_description['eventId'] if alarm_description['eventId'] != -1 else Z3)) \
        & (LogMixin.taskCategory[Y]==(alarm_description['taskCategory'] if alarm_description['taskCategory'] != '' else Z4)) \
        & (LogMixin.message[Y]==(log['message'] if alarm_description['message'] else Z5)) \
        & (LogMixin.timestamp[Y] > current_time - alarm_description['time']))
    evaluation = (count_generic['']==W)
    result = evaluation[0][0] >= alarm_description['count'] if len(evaluation) > 0 else False

    alarms[alarm_index].active = result

    return result


if __name__ == "__main__":

    logs = [
        {
            "agentId": 1,
            "level": 'INFO',
            "timestamp": datetime.datetime.now().timestamp() * 1000,
            "source": 'SHIT',
            "eventId": 1,
            "taskCategory": 'Register',
            "message": 'pera'
        },
        {
            "agentId": 1,
            "level": 'INFO',
            "timestamp": datetime.datetime.now().timestamp() * 1000,
            "source": 'NOTSHIT',
            "eventId": 2,
            "taskCategory": 'Register',
            "message": 'pera'
        },
        {
            "agentId": 1,
            "level": 'INFO',
            "timestamp": datetime.datetime.now().timestamp() * 1000,
            "source": 'NOTSHIT',
            "eventId": 2,
            "taskCategory": 'Register',
            "message": 'pera'
        },
        {
            "agentId": 1,
            "level": 'INFO',
            "timestamp": datetime.datetime.now().timestamp() * 1000,
            "source": 'NOTSHIT',
            "eventId": 3,
            "taskCategory": 'Register',
            "message": 'mika'
        }
    ]

    result = fire_all_rules(logs[0], logs)
    print(result)
    