import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import { Redirect } from 'react-router-dom'
import certificateService from '../../Services/certificateService';

class CertificateDetail extends Component {
  constructor(props) {
    super(props);
    this.subjectKeyIdentifier = '';
    this.state = {
      detail: {},
    };
  }

  async componentDidMount() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('pki_admin'))
    {
      return;
    }
    const { match: { params } } = this.props;
    this.subjectKeyIdentifier = params.subjectKeyIdentifier;
    let detailedNodeData = await certificateService.getById(this.subjectKeyIdentifier);
    let detailedNode = detailedNodeData.data;
   
    this.setState({
      detail: detailedNode,
    })  
  }

  revokeCert = async (e) => {
    await certificateService.revoke(this.state.detail.subjectKeyIdentifier);
    let detailedNodeData = await certificateService.getById(this.subjectKeyIdentifier);
    let detailedNode = detailedNodeData.data;
   
    this.setState({
      detail: detailedNode,
    })  
  }

  render() {
    if (!localStorage.getItem('identity') || !JSON.parse(localStorage.getItem('identity')).roles.includes('pki_admin'))
    {
      return <Redirect to='/unauthorized' />
    }
    return (
      
      <div className="container-fluid">
        <div className="row">
          {this.state.detail && this.state.detail.subject &&  
          <div className= "offset-md-2 col-md-8 pr-0 col-12">
              <div className="pb-5 card col-12" style={{ height: 'fit-content' }}>
                <div className="row">
                  <div className="col-8">
                  {this.state.detail.subject.split('emailAddress=')[0].split`,`.map(e => (
                <h2 className="my-2">{e}</h2>
                ))}
                <p>{new Date(this.state.detail.not_before).toLocaleDateString()} - {new Date(this.state.detail.not_after).toLocaleDateString()}</p>
                  </div>
                  <div  className="col-3 my-auto " >
                    {this.state.detail.revoked && 
                      <img src="/revoked_stamp.svg" style={{width: '100%', height:'auto', 'transform': `rotate(13deg)`}}></img>
                    }
                  </div>

                </div>
                
                <hr className="my-4"></hr>
                <div className="my-4"><h3 className="d-inline">Issued to: </h3><p className="d-inline">{this.state.detail.subject}</p></div>
                <div className="my-4"><h3 className="d-inline">Issued by: </h3><p className="d-inline">{this.state.detail.issuer}</p></div>
                <div className="my-4"><h3 className="d-inline">Valid from: </h3><p className="d-inline">{new Date(this.state.detail.not_before).toLocaleDateString()}</p></div>
                <div className="my-4"><h3 className="d-inline">Valid to: </h3><p className="d-inline">{new Date(this.state.detail.not_after).toLocaleDateString()}</p></div>
                <hr className="my-4"></hr>
                <h3 className="my-4">Subject key identifier: </h3><p>{this.state.detail.subjectKeyIdentifier}</p>
                <br className="mb-5"></br>
                <div className="container">
                  <div className="row">
                    <div className="btn-group blocks col-md-4 offset-md-4 col-sm-6 offset-sm-3 col-8 offset-2" >
                      <button className="col-12 btn btn-danger" onClick={this.revokeCert} disabled={this.state.detail.revoked}>Revoke</button>
                    </div>
                  </div>
                </div>
              </div>
              <div>
              </div>
          </div> 
          }
        </div>
        </div>
        
    );
  }
}

export default CertificateDetail;