import requests
from os import path
import json
import os
import OpenSSL
import datetime
import sys
import ssl
import time
import _thread
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler, FileSystemEventHandler
import re


CERTIFICATE_DIRECTORY = './certificates/'
PKI_URL = 'https://localhost:5000/'
SIEM_CENTER_URL = 'https://localhost:5002/'
# CA_SKID = '06:D4:5B:25:5C:74:48:DA:65:54:70:F5:3F:A8:86:5B:3B:7C:3F:0E'
SIEM_CENTER_NAME = 'O=DefenceFirst, OU=SIEM_CENTER, CN=defencefirst, C=RS, ST=Vojvodina'
DOMAIN_NAME = 'localhost'
PASSWORD = 'agent'
agent_id = None
PATHS = []

BATCH_MODE = False
BATCH_TIME = 1
IS_WINDOWS = False
WIN_LOG_TYPE = 'Application'
WIN_LOG_SERVER = 'localhost'
regx = '.+'

logs_batch = []


def dict_to_string(dico):
    return "|".join(dico.values())

def watch_windows_logs():
    import win32evtlog
    import win32event

    logtype = WIN_LOG_TYPE
    server = None if WIN_LOG_SERVER == '' else WIN_LOG_SERVER

    DELAY = 2


    hand=win32evtlog.OpenEventLog(server,logtype)
    total=win32evtlog.GetNumberOfEventLogRecords(hand)
    flags = win32evtlog.EVENTLOG_BACKWARDS_READ # read events backwards
    
    current = total # send only live logs

    agent_id = 1
    
    while True:
        # if no new events, wait for delay
        total=win32evtlog.GetNumberOfEventLogRecords(hand)
        if total == current:
            time.sleep(DELAY)
            continue
        
        # read events
        events = win32evtlog.ReadEventLog(hand, flags, 0)

        # send recent events
        for ev_obj in events:
            the_time=ev_obj.TimeGenerated.Format() #'12/23/99 15:54:09'
            computer=str(ev_obj.ComputerName)
            cat=ev_obj.EventCategory
            seconds=date2sec(the_time)
            record=ev_obj.RecordNumber
            msg=str(win32evtlogutil.SafeFormatMessage(ev_obj, logtype))
            source=str(ev_obj.SourceName)
            print(the_time, evt_id, computer, cat, seconds, record, msg, source)

            log_dict = {
                "agentId": agent_id,
                "level": 'INFO',
                "timestamp": seconds,
                "source": source,
                "eventId": evt_id,
                "taskCategory": cat,
                "message": msg
            }

            current += 1

            if total == current:
                break
            
            if not BATCH_MODE:
                post_body = { 'logs': [log_dict ]}
                if(re.match(regx, dict_to_string(log_dict))):
                    response = requests.post(SIEM_CENTER_URL+'log', json=post_body, cert=cert, verify=CERTIFICATE_DIRECTORY + 'bundle.crt')
                    print('Windows log sent', post_body, response.json())
            else:
                if(re.match(regx, dict_to_string(log_dict))):
                    logs_batch.append(log_dict)




def log_line_to_dict(line):
        values = line.split('|')
        dictory = {
            "agentId": agent_id,
            "level": values[0] if values[0] != '' else 'WARN',
            "timestamp": int(values[1] if values[1] != '' else datetime.datetime.now().timestamp() * 1000),
            "source": values[2] if values[2] != '' else 'UNKNOWN',
            "eventId": int(values[3]) if values[3] != '' else -1,
            "taskCategory": values[4] if values[4] != '' else 'UNKNOWN',
            "message": values[5]
        }
        return dictory

def periodically_send_request():
    global logs_batch
    while True:
        time.sleep(BATCH_TIME)
        if len(logs_batch) == 0:
            continue
        post_body = {'logs': logs_batch}
        requests.post(SIEM_CENTER_URL+'log', json=post_body, cert=cert, verify=CERTIFICATE_DIRECTORY + 'bundle.crt')
        logs_batch = []
        print('batch sent')

class LogEventHandler(FileSystemEventHandler):
    def on_any_event(self, event):
        if not path.exists(event.src_path):
            return
        with open(event.src_path, 'r') as f:
            lines = f.read().splitlines()
            if len(lines) == 0:
                return
            last_line = lines[-1]
            if(re.match(regx, last_line)):
                post_body = {'logs': [log_line_to_dict(last_line)]}
                r = requests.post(SIEM_CENTER_URL+'log', json=post_body, cert=cert, verify=CERTIFICATE_DIRECTORY + 'bundle.crt')
                print('Log sent ', post_body, r.json())

class BatchEventHandler(FileSystemEventHandler):
    def on_any_event(self, event):
        global logs_batch
        if not path.exists(event.src_path):
            return
        with open(event.src_path, 'r') as f:
            lines = f.read().splitlines()
            if len(lines) == 0:
                return
            last_line = lines[-1]
            if(re.match(regx, last_line)):
                post_body = {'logs': [log_line_to_dict(last_line)]}
                logs_batch.append(log_line_to_dict(last_line))
            


with open("settings.json", "r") as settingsF:
    settings = json.loads(settingsF.read())
    agent_id = settings['id']
    PASSWORD = settings['password']
    PATHS = settings['paths']
    BATCH_MODE = settings['batchMode']
    BATCH_TIME = settings['batchTime']
    IS_WINDOWS = settings['Windows']
    WIN_LOG_TYPE = settings['WinLogType']
    WIN_LOG_SERVER = settings['WinLogServer']
    regx = settings['regex']
    

# REQUEST AGENT AND OBTAIN CERTIFICATE  
    
if not path.exists(CERTIFICATE_DIRECTORY):
    os.mkdir(CERTIFICATE_DIRECTORY)

if not path.exists(CERTIFICATE_DIRECTORY + '/cert.crt'):



    if agent_id == -1:
        post_body = {
            'siem_center_name': SIEM_CENTER_NAME
        }

        result = requests.post(PKI_URL + 'agent', json=post_body, verify=CERTIFICATE_DIRECTORY + 'rootCA.crt')
        agent_id = result.json()['id']
    
        with open("settings.json", "r") as f:
            jsondumpling = json.loads(f.read())
        jsondumpling['id'] = agent_id    
        with open("settings.json", "w") as f:
            f.write(json.dumps(jsondumpling, indent=4))

    subject_dict = {
        'organization': 'Defence First',
        'organizationalUnit': 'SIEMagent' + str(agent_id),
        'commonName': DOMAIN_NAME,
        'country': 'RS',
        'state': 'Vojvodina'
    }
    key = OpenSSL.crypto.PKey()
    key.generate_key(OpenSSL.crypto.TYPE_RSA, 2048)
    private_key = OpenSSL.crypto.dump_privatekey(OpenSSL.crypto.FILETYPE_PEM, key).decode('ascii')
    public_key = OpenSSL.crypto.dump_publickey(OpenSSL.crypto.FILETYPE_PEM, key).decode('ascii')

    post_body = {
        'subjectName': subject_dict,
        'publicKey': public_key,
        'id': agent_id,
        'password': PASSWORD
    }

    while(True):
        result = requests.post(PKI_URL + 'agent/certificate', json=post_body, verify=CERTIFICATE_DIRECTORY + 'rootCA.crt')
        status = result.json()['status']
        print(status)
        if status:
            break
        time.sleep(10)

    

    result = result.json()
    certificate = result['cert']
    bundle = result['bundle']

    f_key = open(CERTIFICATE_DIRECTORY + 'cert.key', 'w')
    f_key.write(private_key)
    f_key.close()

    f_cert = open(CERTIFICATE_DIRECTORY + 'cert.crt', 'w')  
    f_cert.write(certificate)
    f_cert.close()

    f_bundle = open(CERTIFICATE_DIRECTORY + 'bundle.crt', 'w')
    f_bundle.write(bundle)
    f_bundle.close()

# print(ssl.get_server_certificate(('localhost', '5002'), ca_certs=CERTIFICATE_DIRECTORY + 'siem_ca.crt'))

cert = (CERTIFICATE_DIRECTORY + 'cert.crt', CERTIFICATE_DIRECTORY + 'cert.key')

response = requests.get(PKI_URL + 'siem/' + SIEM_CENTER_NAME + '/revoked', verify=CERTIFICATE_DIRECTORY + 'rootCA.crt')
revoked = response.json()['revoked']
if revoked:
    print('SIEM center certificate revoked')
    exit()

r=requests.get('https://localhost:5002/', cert=cert, verify=CERTIFICATE_DIRECTORY + 'bundle.crt')

print(r)


# OBSERVE LOGS

if IS_WINDOWS:
    _thread.start_new_thread(watch_windows_logs, ())

if BATCH_MODE:
    _thread.start_new_thread(periodically_send_request, ())

observers = []

for pathra in PATHS:
        
    if BATCH_MODE:
        event_handler = BatchEventHandler()
    else:
        event_handler = LogEventHandler()   

    observer = Observer()
    observer.schedule(event_handler, pathra, recursive=False)
    observer.start()
    observers.append(observer)

try:
    while True:
        time.sleep(1)
except KeyboardInterrupt:
    for observer in observers:
        observer.stop()
for observer in observers:
    observer.join()