import time
from datetime import datetime
import json 
file_path = './'
interval = 4
STATES_DIR = './states/'


with open("settings.json", "r") as settingsF:
    settings = json.loads(settingsF.read())
    file_path = settings['file_path']
    interval = settings['interval']
    state_path = settings['state_path']

print('state', state_path)

while True:
    with open(STATES_DIR + state_path, 'r') as simulatra:
        for line in simulatra:
            with open(file_path, 'a') as f:
                splited = line.split('|')
                dt = datetime.now().timestamp()*1000 + int(splited[1])
                splited[1] = str(int(dt))
                f.write('|'.join(splited))
            time.sleep(interval)