from jsonschema import validate
from jsonschema.exceptions import ValidationError
from werkzeug.exceptions import BadRequest


def validate_ca(json_data):
    schema = {
        'type': 'object',
        "required": [ 'subjectName', 'duration' ],
        'properties': {
            'subjectName': {
                'type': 'object',
                'properties': {
                    'country': {
                        'type': 'string',
                        'minLength': 2,
                        'maxLength': 2
                    },
                    'state': {
                        'type': 'string'
                    },
                    'locality': {
                        'type': 'string'
                    },
                    'organization': {
                        'type': 'string'
                    },
                    'organizationalUnit': {
                        'type': 'string'
                    },
                    'commonName': {
                        'type': 'string'
                    },
                    'email': {
                        'type': 'string',
                        # RFC5222 email regex
                        'pattern': '(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.)\{3\}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])'
                    }
                }
            },
            'duration': {
                'type': 'number',
                'minimum': 1,
                'maximum': 20
            }
        }
    }
    try:
        return validate(json_data, schema)
    except ValidationError as err:
        raise BadRequest(description=err.message)

def validate_rule(json_data):
    schema = {
        'type': 'object',
        'required': ['agentId', 'level', 'source', 'eventId', 'taskCategory', 'message', 'time', 'count', 'info_message'],
        'properties':{
            'agentId': {
                'type': 'boolean'
            },
            'level': {
                'type': 'string'
            },
            'source': {
                'type': 'boolean'
            },
            'eventId': {
                'type': 'number'
            },
            'taskCategory': {
                'type': 'string'
            },
            'message': {
                'type': 'boolean'
            },
            'time': {
                'type': 'number'
            },
            'count': {
                'type': 'number'
            },
            'info_message': {
                'type': 'string'
            }

        }
    }
    try:
        return validate(json_data, schema)
    except ValidationError as err:
        raise BadRequest(description=err.message)

def validate_add_siem_center(json_data):
    schema = {
        'type': 'object',
        'required': ['name', 'parent', 'password'],
        'properties':{
            'name': {
                'type': 'string'
            },
            'parent': {
                'type': 'number'
            },
            'password': {
                'type': 'string'
            }
        }
    }
    try:
        return validate(json_data, schema)
    except ValidationError as err:
        raise BadRequest(description=err.message)

def validate_lt_gt(json_data):
    schema = {
        'type': 'object',
        'required': ['lt', 'gt'],
        'properties': {
            'lt': {
                'type': 'number'
            },
            'gt': {
                'type': 'number'
            }
        }
    }
