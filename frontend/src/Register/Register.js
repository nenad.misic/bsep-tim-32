import React, { useState } from "react";
import { Button, FormGroup, FormControl } from "react-bootstrap";

export default function Register() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [role, setRole] = useState("");

  function validateForm() {
    return username.length > 0 && password.length > 0 && role;
  }

  function handleSubmit(event) {
    event.preventDefault();
  }

 

  return (
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="username" bssize="large">
          <label>Username</label>
          <FormControl
            autoFocus
            type="username"
            value={username}
            onChange={e => setUsername(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password" bssize="large">
          <label>Password</label>
          <FormControl
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
        <FormGroup controlId="exampleForm.ControlSelect1" id="roleSelect" onChange={e => setRole(e.target.value)}>
            <label>Role</label>
            <FormControl as="select">
                <option>Regular user</option>
                <option>Admininstrator</option>
            </FormControl>
        </FormGroup>
        <Button className="my-5" block bssize="large" disabled={!validateForm()} type="submit">
          Register
        </Button>
      </form>
    </div>
  );
}
