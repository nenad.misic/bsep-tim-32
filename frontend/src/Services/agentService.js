import axios from 'axios';
import {api_siem, bearer} from '../Environment/environment';
import { resolve } from 'dns';

let agentService = {
    getAll: async (id) => {
        // let url = `${api_pki}ca`;
        // return axios.get(url);
        let res = await axios.get(`${api_siem}agent/${id}`, bearer())
        return res
    },
    getAllForMe: async () => {
        // let url = `${api_pki}ca`;
        // return axios.get(url);
        // TODO
        let agents = await axios.get(`${api_siem}agent/${JSON.parse(localStorage.getItem('identity')).siem_center_id}`, bearer())
        return agents
    },
    getLogsForAgent: (agid) => {
        console.log(agid);
        let res = axios.get(`${api_siem}log/${agid}`, bearer())
        return res
    },
    getAlarmsForAgent: (agid) => {
        console.log(agid);
        let res = axios.get(`${api_siem}alarm/${agid}`, bearer())
        return res
    },
    getLogsForAgentDate: (agid, gt, lt) => {
        let res = axios.post(`${api_siem}logDate/${agid}`, {lt: lt.getTime(), gt: gt.getTime()}, bearer())
        return res
    },
    getAlarmsForAgentDate: (agid, gt, lt) => {
        let res = axios.post(`${api_siem}alarmDate/${agid}`, {lt: lt.getTime(), gt: gt.getTime()}, bearer())
        return res
    },
    buildTree: (data) => {
        let root = {title: "Agents", disabledDetails: true, children: data};
        return root;
    },
    approveAgent: (agid, password) => {
        let res = axios.post(`${api_siem}agent`, {id: agid, password}, bearer())
        return res
    }
    
};

export default agentService

